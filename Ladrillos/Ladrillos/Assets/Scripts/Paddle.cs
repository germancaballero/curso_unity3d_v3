﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    //Creamos la variable de velocidad del paddle y le establecemos una velocidad de 5
    [SerializeField] float speed = 5;
    //Creamos la variable GameManager para usarla
    GameManager gameManager;
    //Creamos la variable de los límites del Paddle para que no se salga de los límites
    [SerializeField] float xLimit = 7.5f;
    //Creamos la variable de los límites del Paddle cuando sea grande para que no se salga de los límites
    [SerializeField] float xLimitWhenBig = 6.3f;
    //Creamos la variable pública para controlar el tiempo del crecimiento del paddle
    [SerializeField] byte bigSizeTime = 10;
    //Creamos la variable pública para controlar el tiempo de la super bola
    [SerializeField] byte superBallTime = 10;
    //Creamos la variable pública para controlar el tiempo del laser
    [SerializeField] byte laserTime = 10;
    //Creamos el objeto del prefab de la bala
    [SerializeField] GameObject bulletPrefab;
    //Posición de inicio de disparo
    [SerializeField] Vector3 shootingOffset;

    // Start is called before the first frame update
    void Start()
    {
        //Buscamos el objeto al comenzar
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //Preguntamos si le damos a la tecla d y el límite es menor que el que le hemos establecido
        if (Input.GetKey(KeyCode.D)&& transform.position.x < xLimit)
        {
            //Le damos movimiento y velocidad
            transform.position += speed * Time.deltaTime * Vector3.right;
        }
        //Preguntamos si le damos a la tecla a y el límite es mayor que el que le hemos establecido
        if (Input.GetKey(KeyCode.A) && transform.position.x > -xLimit)
        {
            //Le damos movimiento y velocidad
            transform.position += speed * Time.deltaTime * Vector3.left;
        }
        //PReguntamos si le hemos hecho clic en el izdo. del ratón
        if (Input.GetMouseButtonDown(0))
        {
            //Si la bola no está en juego
            if (gameManager.BallOnPlay == false)
            {
                //Le decimos que empiece a moverse
                gameManager.BallOnPlay = true;
            }
            //Si el juego no está funcionando
            if (gameManager.Gamestarted == false)
            {
                //Ponemos el juego en funcionamiento
                gameManager.Gamestarted = true;
            }
        }
    }
    //Creamos una clase OnTrigger para controlar que cuando el efecto del power choque con el paddle se destruya
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Preguntamos si colisiona con el powerUp
        if (collision.CompareTag("PowerUp"))
        {
            //Preguntamos si el componente que nos golpea es de tipo PowerUp paddle grande , y si lo es entramos
            if (collision.GetComponent<PowerUp>().powerUpType == PowerUp.PowerUpType.IncreaseSize)
            {
                //Le decimos que sí es.
                gameManager.bigSize = true;
                //Llamamos a la coroutine para que actue
                StartCoroutine(BigSizePower());

            }
            /*********************************************
            
            //Preguntamos si el componente que nos golpea es de tipo PowerUp super bola, y si lo es entramos
            else if (collision.GetComponent<PowerUp>().powerUpType == PowerUp.PowerUpType.SuperBall)
            {
                //Le decimos que sí es.
                gameManager.SuperBall = true;
                //Llamamos a la coroutine para que actue
                StartCoroutine(StopSuperBall());
                
            }//Preguntamos si el componente que nos golpea es de tipo PowerUp laser , y si lo es entramos
            ************************************************/
            else if (collision.GetComponent<PowerUp>().powerUpType == PowerUp.PowerUpType.Laser)
            {
                //Le decimos que sí es.
                gameManager.laser = true;
                //Llamamos a la coroutine para que actue
                StartCoroutine(ContinuousShooting());

            }
            //Si colisiona lo destruimos
            Destroy(collision.gameObject);
        }

    }
    //Creamos la Coroutine
    IEnumerator BigSizePower()
    {
        //Guardamos el límite cuando es pequeño el paddle
        float originalXLimit = xLimit;
        //Le asignamos el valor del límite cuando el paddle es grande
        xLimit = xLimitWhenBig;
        //Creamos el objeto para poder usarlo
        Vector3 newSize = transform.localScale;
        while (transform.localScale.x < 1)
        {
            newSize.x += Time.deltaTime;
            transform.localScale = newSize;
        }
        //Tiempo de Incrementar tamaño
        yield return new WaitForSeconds(bigSizeTime);
        //Reducir tamaño
        while (transform.localScale.x > 0.5)
        {
            newSize.x -= Time.deltaTime;
            transform.localScale = newSize;
        }
        //Finalizamos el tamaño grande
        gameManager.bigSize = false;
        //Le ponemos los límtes originales del paddle
        xLimit = originalXLimit;
    }
    //Creamos la Coroutine
    IEnumerator StopSuperBall()
    {
        //Tiempo de ser super bola
        yield return new WaitForSeconds(superBallTime);
        //Desactivamos la super bola
        gameManager.SuperBall = false;

    }
    //Creamos la Coroutine
    IEnumerator ContinuousShooting()
    {
        float stopShootingTime = Time.time + laserTime;
        while (Time.time < stopShootingTime)
        {
            //Creamos el disparo
            Instantiate(bulletPrefab, transform.position + shootingOffset, Quaternion.identity);
            //Tiempo entre balas
            yield return new WaitForSeconds(1);
        }
    }
}
