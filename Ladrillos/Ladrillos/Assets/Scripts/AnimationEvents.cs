﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    //Creamos la variable para el sonido de la explosión
    [SerializeField] AudioClip explosion;
    //Destruir la animación de la explosión
    public void DestroyExplosion()
    {
        Destroy(gameObject);
    }

    //
    public void SendExplosionSound()
    {
        FindObjectOfType<AudioController>().PlaySfx(explosion);
    }
}
