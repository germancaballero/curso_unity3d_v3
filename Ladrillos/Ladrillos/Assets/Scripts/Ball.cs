﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    //Hace visible la variable en el Script unity el objeto que está asignado en el Gigibody 2D
    [SerializeField] Rigidbody2D rigibody2d;
    //Creamos variable de dirección de la bola en el rebote
    Vector2 moveDirection;
    //Creamos la variable para gestión de velocidad.
    Vector2 currentVelocity;
    //Mostramos y creamos la variable velocidad con un valor de 3
    [SerializeField] float speed = 3;
    //Creamos la variable de la clase GameManager
    GameManager gameManager;
    //Creamos la variable del sonido que hace el paddle cuando le da a la bola y poder asignarlo en la bola en el script su sonido.
    [SerializeField] AudioClip paddleBounce;
    //Creamos una variable para que almacene el audio del clip
    [SerializeField] AudioClip bounce;
    //Creamos la variable para que almacene el sonido de perder una bola.
    [SerializeField] AudioClip loseLife;
    //Creamos una variable para controlar la velocidad mínima de y
    [SerializeField] float yMinSpeed = 2;
    //Variable que guarda la referencia al componente Trail Renderer que manejamos en la super bola
    [SerializeField] TrailRenderer trail;
    //Variable Audiocontroler
    AudioController audiocontroller;

    //Método para la propiedad de super ball
    public void ToggleTrail()
    {
        if (trail.enabled == true)
        {
            trail.enabled = false;
        } else
        {
            trail.enabled = true;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //Como obtener el objeto
        //rigibody2d = GetComponent<Rigidbody2D>();
        //Como darle velocidad a la bola
        //rigibody2d.velocity = Vector2.up * speed;

        //Buscamos el objeto GameManager
        gameManager = FindObjectOfType<GameManager>();
        audiocontroller = FindObjectOfType<AudioController>();
    }


    private void FixedUpdate()
    {
        //Le damos el valor de la velocidad
        currentVelocity = rigibody2d.velocity;
    }
    //Cuando la bola colisiona con un objeto
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Preguntamos si choca con un brick y si superball es true
        if(collision.transform.CompareTag("Brick")&& gameManager.SuperBall == true)
        {
            //le asignamos la velocidad actual
            rigibody2d.velocity = currentVelocity;
            //Evita que se ejecute el resto del código
            return;
        }

        //Le damos la dirección de rebote a la bola
        moveDirection = Vector2.Reflect(currentVelocity, collision.GetContact(0).normal);
        //Preguntamos por la bola en juego
        if (gameManager.BallOnPlay)
        {
            //si la velocidad actual absoluta(sin signo) en Y es menor que la velocidad mínima
            if (Mathf.Abs(moveDirection.y)< yMinSpeed)
            {
                //Le asignamos la velocidad mínima por la que tiene
                moveDirection.y = yMinSpeed * Mathf.Sign(moveDirection.y);
            }
        }
        //Le asignamos la dirección a la bola.
        rigibody2d.velocity = moveDirection;
      
        //Si el objeto al que golpea se llama en su target DeathLimit
        if (collision.transform.CompareTag("DeathLimit"))
        {
            //Debug.Log("Colisión con el límite bajo.");
            //Buscamos el objeto de pérdida de vida. 
            FindObjectOfType<AudioController>().PlaySfx(loseLife);
            //Le quita una vida
            gameManager.PlayerLive--;
        }
        //Preguntamos si existe el paddle preguntando por su nombre 
        if (collision.transform.CompareTag("Player"))
        {
            //Buscamos el objeto de colisión con el paddle.
            FindObjectOfType<AudioController>().PlaySfx(paddleBounce);
        }
        if (collision.transform.CompareTag("Brick"))
        {
            //Buscamos el objeto de colisión con el ladrillo.
            FindObjectOfType<AudioController>().PlaySfx(bounce);
        }
    }
    //Función para empezar a mover la bola
    public void LaunchBall()
    {
        //La bola deja de ser hija del paddle
        transform.SetParent(null);
        //Se le dice que se mueva a una velocidad hacia arriba
        rigibody2d.velocity = Vector2.up * speed;
    }
    //Reiniciamos la bola si se ha destruido
    public void ResetBall()
    {   //Buscamos el objeto Paddle y lo guardamos
        Transform paddle = GameObject.Find("Paddle").transform;
        paddle.localScale = new Vector3(0.5f, 1f, 1f);
        //Ponemos la velocidad a 0
        rigibody2d.velocity = Vector3.zero;
        
        //Establecemos a paddle como padre de la bola
        transform.SetParent(paddle);
        //Le asignamos a la bola la posición del paddle
        Vector2 ballPosition = paddle.position;
        //Lo elevamos para que tenga su posición de inicio.
        ballPosition.y += 0.5f;
        //Le asignamos dicha posición
        transform.position = ballPosition;
        //Establecemos que la bola no está en juego.
        gameManager.BallOnPlay = false;
    }
}
