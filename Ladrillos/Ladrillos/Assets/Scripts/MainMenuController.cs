﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    bool loadingScene;
 public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }
   public void QuitGame()
    {
        Application.Quit();
    }
    IEnumerator LoadGameScene()
    {
        loadingScene = true;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Game");
    }
   
}
