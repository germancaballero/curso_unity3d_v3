﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    //GameObject gameManagerObj;
    GameManager gameManager;
    //Creamos el objeto para crear un prefab para disponer de la explosión durante el juego. En el inspector del prefab Brick aparece y hay que llevarle el prefab de la explosión
    [SerializeField] GameObject explosionPrefab;
    //Creamos el array para crear un prefab para disponer de los powersUps
    [SerializeField] GameObject [] powerUpsPrefabs;
    //Creamos la variable de posibilidades del porcentaje de los powerups
    [SerializeField] byte powerUpPossibilityPercentage = 10;
    private void Start()
    {
        /*Lo mismo que lo de abajo
         * gameManagerObj = GameObject.Find("GameManager");
        if (gameManagerObj == null)
        {
            Debug.Log("Objeto no encontrado");
        }
        else
        {
            gameManager = gameManagerObj.GetComponent<GameManager>();
            gameManager.bricksOnLevel++;
        }*/
        //Buscamos y cogemos el ladrillo
        gameManager = FindObjectOfType<GameManager>();
        //Preguntamos si lo hemos encontrado
        if (gameManager != null)
        {
            //los contamos
            gameManager.BricksOnLevel++;
        }
    }
    //Creamos una función que descuenta un ladrillo cuando la bola choca con él
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Creamos el prefab en el momento de la colisión con el ladrillo
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
       //Preguntamos si existe al chocar
        if (gameManager != null)
        {
            //Lo restamos del número de ladrillos total
            gameManager.BricksOnLevel--;
        }

        //Preguntamos si no hay un power up
        if (gameManager.bigSize == false && gameManager.SuperBall == false && gameManager.laser == false)
        {
            //Número aleatorio entre 0 y 99
            int randomNumber = Random.Range(0, 100);
            //Preguntamos si entramos a crear el powerup porque el número es menor que el que marquemos
            if (randomNumber < powerUpPossibilityPercentage)
            {
                //Crear un powerUp
                int randomPower = Random.Range(0, powerUpsPrefabs.Length);
                Instantiate(powerUpsPrefabs[randomPower], transform.position, Quaternion.identity);
                
            }
        }
       
        //Lo destruimos
        Destroy(gameObject);
    }

}
