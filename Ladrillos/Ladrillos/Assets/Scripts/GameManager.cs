﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Para calcular el tiempo
    float gameTime;
    //Número de ladrillos
    [SerializeField] byte bricksOnLevel;
    //Creamos la variable boleana para controlar el PowerUp del paddle grande
    public bool bigSize;
    //Creamos la variable boleana para controlar el PowerUp de la super bola
    private bool superBall;
    //Creamos la variable boleana para controlar el PowerUp del laser
    public bool laser;
    //Creamos una variable de tipo UiControler
    UIController uiController;
    //Creamos una variable de tipo ball
    Ball ball;

    private void Start()
    {
        uiController = FindObjectOfType<UIController>();
        ball = FindObjectOfType<Ball>();
    }
    //Propiedad se SuperBall
    public bool SuperBall
    {
        
        get => superBall;
        //se encarga de asignar el valor de la super ball y ejecutar el método ToggleTrail
        set
        {
            superBall = value;
            ball.ToggleTrail();
        }
    }
    //Propiedades del nivel
    public byte BricksOnLevel
    {
        //Cogemos el valor de nivel 
        get => bricksOnLevel;
        //Establecemos los valores
        set
        {
            //
            bricksOnLevel = value;
            //Si el valor es distinto de 0 entramos
            if (bricksOnLevel == 0)
            {

                Debug.Log("Has ganado el nivel");
                //Destruimos la bola.
                Destroy(GameObject.Find("Ball"));
                //Obtenemos el tiempo total de juego, restando el tiempo actual con el tiempo de comienzo.
                gameTime = Time.time - gameTime;
                //Mostrar pantalla de victoria. Y mandamos el tiempo
                uiController.ActivateWinnerPanel(gameTime);
                
            }
        }  
    }
    //Creamos las variable de vidas y le damos el valor 3
   [SerializeField] byte playerLives = 3;
    //Propiedades de la vida
    public byte PlayerLive
    {
        //Cogemos el valor
        get => playerLives;
        set {
            //Le damos el valor de 3 vidas
            playerLives = value;
            //Preguntamos si es igual a 0
            if (playerLives == 0)
            {
                //Destruimos la bola
                Destroy(ball.gameObject);
                //Mostrar pantalla de derrota.
                uiController.ActivateLosePanel();
            }
            else
            {
                //Buscamos la bola y la reseteamos a su posición inicial.
                ball.ResetBall();
            }
            //Actualizamos las vidas enviando la cantidad e vidas que hay
            uiController.UpdateUILives(playerLives);
        }
    }
    //Creamos la variable del inicio del juego
    [SerializeField] bool gameStarted;
    public bool Gamestarted
    {
        // Cogemos el valor
        get => gameStarted;
        // Le damos el valor.
        set
        {
            //Le damos valor de inicio.
            gameStarted = value;
            //Le damos el tiempo de inicio a la variable.
            gameTime = Time.time;
        }
    }

    //Creamos la variable de la bola en el juego
    [SerializeField] bool ballOnPlay;
    public bool BallOnPlay
    {
        //Cogemos el valor
        get => ballOnPlay;
        //Le damos el valor
        set
        {
            ballOnPlay = value;
            //si el vlaor es verdadero
            if (ballOnPlay == true)
            {
                Debug.Log("Lanzar la bola");
                //Buscamos el objeto llamado Ball y llamamos a la función LaunchBall()
                ball.LaunchBall();
            }
        }
    }
}
