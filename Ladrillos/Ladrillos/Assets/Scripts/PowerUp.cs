﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    //Declaración de un enum, que es como una propiedad pero solo se escribe la lista de cosntantes.
    //Ahora se crea un sprite llamado PowerUp y le añadimos el sprite símbolo + y le pondremos el Tag PowerUp y le añadimos un boxcollaider2d para la colisión con el paddle activando is Trigger para no empujar a otros objetos. Añadimos el Script.
    // Al paddle hay que añadirle un RigiBody 2d para que interctaue con el PowerUp para eso hay que ponerlo como "Kinematic".
    public enum PowerUpType
    {
        IncreaseSize,
        SuperBall,
        Laser
    }
    //Creamos variable pública para mostrar los powers up.
    public PowerUpType powerUpType;
    //Creamos una variable para la velocidad de caida.
    [SerializeField] float speed = 5;
    //Destruimos los objetos de powerups que no se recogan a los 10 segundos
    private void Start()
    {
        Destroy(gameObject, 10);
    }
    private void Update()
    {
        //Le damos velocidad a la caida del efecto del paddle
        transform.position += Time.deltaTime * Vector3.down * speed;
    }
}
