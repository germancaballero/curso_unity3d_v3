﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    //Creamos el array de efectos especiales para poder añadir los efectos en el controlador de audio
    [SerializeField] AudioSource [] sfxChannel;
    WaitForSeconds cleanTime = new WaitForSeconds(5);

    //Creamos la clase para control de los efectos especiales.
public void PlaySfx(AudioClip clip)
    {
        //Asignamos el efecto de sonido.
        //sfxChannel.clip = clip;
        //y lo activamos para que suene.
        //sfxChannel.Play();


        //Recorremos el array para buscar el que esté vacio para asignarle un sonido
        for(int i=0; i < sfxChannel.Length; i++)
        {
            //Si el canal es nulo está disponible y se le asigna un sonido
            if(sfxChannel[i].clip == null)
            {
                //Asignamos el sonido
                sfxChannel[i].clip = clip;
                //ejecutamos el sonido
                sfxChannel[i].Play();
                //Enviamos la longitud del array y el índice
                StartCoroutine(CleanAudioChannel( i));
                //Paramos el for porque ya hemos encontrado donde asignar el sonido.
                break;
            }
        }
    }
  
    //Declaramos una Corutina y le damos nombre con dos parametros, la cantidad de audios y el canal
    IEnumerator CleanAudioChannel(int channel)
    {
        //Declaramos el tiempo de espera de 3 segundos.
        yield return cleanTime;
        //Establecemos el clip en null para asegurarnos que se queda vacío.
        sfxChannel[channel].clip = null;
    }
}
