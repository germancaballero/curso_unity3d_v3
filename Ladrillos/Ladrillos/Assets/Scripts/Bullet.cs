﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //Creamos la variable de velocidad de la bala
    [SerializeField] float speed = 1;

    // Update is called once per frame
    void Update()
    {
        //le damos movimiento hacia arriba y velocidad
        transform.position += Time.deltaTime * Vector3.up * speed;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //La destruimos al entrar en colisión
        Destroy(gameObject);
    }
}
