﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Añadimos librería para utilizar SceneManagement que nos reinicia todos los elementos del juego.
using UnityEngine.SceneManagement;
//Añadimos librería para utilizar Text
using UnityEngine.UI;



public class UIController : MonoBehaviour
{
    //Variable para control del panel de perdedor
    [SerializeField] GameObject losePanel;
    //Variable para control del panel de ganador
    [SerializeField] GameObject winnerPanel;
    //Variable para control de vidas, es un array que se rellena arrastrando los tres objetos de vida de dentro del canvas al "UIController"=>"Lives Img"
    [SerializeField] GameObject[] livesImg;
    //Variable para mostrar el tiempo
    [SerializeField] Text gameTimeText;
    //añadimos la variable que contendrá el sonido de presionar un botón.
    [SerializeField] AudioClip buttonPress;
    //Variable para evitar que le dé varias veces al botón
    bool loadingScene;


    //Creamos un método para activar el panel de perdedor
    public void ActivateLosePanel()
    {
        losePanel.SetActive(true);
    }

    //Creamos un método para activar el panel de ganador y enviamos el tiempo
    public void ActivateWinnerPanel(float gameTime)
    {
        winnerPanel.SetActive(true);
        //Utilizamos el tiempo recibido para mostrarlo en pantalla y lo redondeamos a enteros. añadimos "using Su¡ystem;" para utilizar la biblioteca Math.
        gameTimeText.text = "Tiempo de juego: " + Math.Floor(gameTime) + " sgs.";
    }

    //Reiniciar el juego
    public void RestartCurrentScene()
    {
        //Preguntamos si le ha dado al botón
        if (loadingScene == true)
        {
            return;
        }
        //Buscamos el botón para asignarle el sonido
        FindObjectOfType<AudioController>().PlaySfx(buttonPress);
        //Cargamos todos los elementos del juego desde su inicio con un pequeño retraso.
        StartCoroutine(LoadNextScene("Game"));
        //Cargamos todos los elementos del juego desde su inicio.
        //SceneManager.LoadScene("Game");
        //Vamos a Menú Unity "File"=>"Build setting.." y ahí desde el Project=>Assets=>Scenes la escena "Game" a la ventana vacía "Scenes in Build".

    }

    //Ir al menú principal
    public void GoToMainMenu()
    {
        //Preguntamos si le ha dado al botón
        if (loadingScene == true)
        {
            return;
        }
        //Buscamos el botón para asignarle el sonido
        FindObjectOfType<AudioController>().PlaySfx(buttonPress);
        //Cargamos todos los elementos del juego desde su inicio con un pequeño retraso.
        StartCoroutine(LoadNextScene("MainMenu"));
        //SceneManager.LoadScene("MainMenu");

    }
    //Añadir un pequeño retraso para el cambio de escena para que suene el sonido del botón entero
    IEnumerator LoadNextScene(string sceneName)
    {
        //Decimos que el botón ha sido dado
        loadingScene = true;
        //le damos un segundo para cargar
        yield return new WaitForSeconds(1);
        //Carga la escena
        SceneManager.LoadScene(sceneName);
    }
    //Método para controlar la cantidad de vidas que se visualizan en pantalla
    public void UpdateUILives(byte currentLives)
    {
        //Recorre el array de imagenes de vida
        for (int i= 0; i < livesImg.Length; i++)
        {
            //Si quedan vidas, quitamos una
            if (i >= currentLives)
            {
                //Cada vez que entra pone en falso una imagen
                livesImg[i].SetActive(false);
            }
        }
    }
    
}
