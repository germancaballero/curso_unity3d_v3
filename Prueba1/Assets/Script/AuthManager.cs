﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using TMPro;

public class AuthManager : MonoBehaviour
{
    //Firebase variables para comunicarse con la base de datos
    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser User;

    //Login variables
    [Header("Login")]
    public TMP_InputField emailLoginField;//Le ponemos el objeto “Email_Input” del “Canvas”
    public TMP_InputField passwordLoginField;//Le ponemos el objeto “Password_Input” del “Canvas”
    public TMP_Text warningLoginText;//Le ponemos el objeto “Warning_Text” del “Canvas”
    public TMP_Text confirmLoginText;//Le ponemos el objeto “Confirm_Text” del “Canvas”

    //Register variables
    [Header("Register")]
    public TMP_InputField usernameRegisterField;//Le ponemos el objeto “Username_Input” del “Canvas”
    public TMP_InputField emailRegisterfield;//Le ponemos el objeto “Email_Input”” del “Canvas”
    public TMP_InputField passwordRegisterField;//Le ponemos el objeto “Password_Input” del “Canvas”
    public TMP_InputField passwordRegisterVerifyField;//Le ponemos el objeto “Confirm_Input” del “Canvas”
    public TMP_Text warningRegisterText;//Le ponemos el objeto “Warning_Text” del “Canvas”

    private void Awake()
    {
        //Verifica que todas las dependencias necesarias para Firebase estén presentes en el sistema

        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //Si están disponibles, inicialice Firebase
                InitializedFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });

    }

    private void InitializedFirebase()
    {
        Debug.Log("Setting up firebase Auth");
        //Establecer el objeto de la instancia de autenticación
        auth = FirebaseAuth.DefaultInstance;
    }

    //Función para el botón de inicio de sesión
    public void LoginButton()
    {
        //Se llama a la corrutina de inicio de sesión pasando el correo electrónico y la contraseña
        StartCoroutine(Login(emailLoginField.text, passwordLoginField.text));
    }

    //Función para el botón de registro 
    public void RegisterButton()
    {
        //Se llama al registro pasando el correo electrónico, la contraseña y el nombre de usuario
        StartCoroutine(Register(emailRegisterfield.text, passwordRegisterField.text, usernameRegisterField.text));
    }

    private IEnumerator Login(string _email, string _password)
    {
        //Se llama a la función de inicio de sesión de autenticación de firebase pasando el correo electrónico y la contraseña

        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);
        //Espere hasta que se complete la tarea
        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

        if (LoginTask.Exception != null)
        {
            //Si hay errores los gestionamos
            Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

            string message = "Login Failed!";
            switch (errorCode)
            {
                case AuthError.MissingEmail:
                    message = "Missing Email";
                    break;
                case AuthError.MissingPassword:
                    message = "Missing Password";
                    break;
                case AuthError.WrongPassword:
                    message = "Wrong Password";
                    break;
                case AuthError.InvalidEmail:
                    message = "Invalid Email";
                    break;
                case AuthError.UserNotFound:
                    message = "User Not Found";
                    break;
            }
            warningLoginText.text = message;
        }
        else
        {
            //El usuario es ahora logeado
            //Ahora obtiene el resultado
            User = LoginTask.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", User.DisplayName, User.Email);
            warningLoginText.text = "";
            confirmLoginText.text = "Logged In";
        }
    }

    private IEnumerator Register(string _email, string _password, string _username)
    {
        if (_username == "")
        {
            //Si el campo de nombre de usuario está en blanco, muestra una advertencia
            warningRegisterText.text = "Missing Username";
        }
        else if (passwordRegisterField.text != passwordRegisterVerifyField.text)
        {
            //Si la contraseña no coincide, muestra una advertencia
            warningRegisterText.text = "Password Does Not Match!";
        }
        else
        {
            //Se llama a la función de inicio de sesión de autenticación de Firebase pasando el correo electrónico y la contraseña
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
            //Espere hasta que se complete la tarea

            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                //Si hay errores los gestionamos
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                string message = "Register Failed";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingPassword:
                        message = "Missing Password";
                        break;
                    case AuthError.WeakPassword:
                        message = "Weak Password";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        message = "Email Already In Use";
                        break;
                }
                warningRegisterText.text = message;
            }
            else
            {
                //el usuario se crea ahora
                //Recogemos el resultado
                User = RegisterTask.Result;

                if (User != null)
                {
                    //Se crea un perfil de usuario y se establece el nombre de usuario
                    UserProfile profile = new UserProfile { DisplayName = _username };

                    //Llame a la función de perfil de usuario de actualización de autenticación de firebase pasando el perfil con el nombre de usuario
                    var ProfileTask = User.UpdateUserProfileAsync(profile);
                    //Espere hasta que se complete la tarea
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        //Si hay errores los gestionamos
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorcode = (AuthError)firebaseEx.ErrorCode;
                        warningRegisterText.text = "Username Set Failed!";
                    }
                    else
                    {
                        //El nombre de usuario ahora está configurado
                        //Ahora regrese a la pantalla de inicio de sesión
                        UIManager.instance.LoginScreen();
                        warningRegisterText.text = "";
                    }
                }


            }
        }
    }
}

