﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptPersonaje : MonoBehaviour
{

    //Variables
    //Creamos el objeto anim del tipo Animator
    Animator anim;
    //Creamos el objeto rb del tipo RigidBody2D
    Rigidbody2D rb;
    //Creamos la variable pública para controlar la fuerza del salto en Unity en el inspector.
    public float fuerzaSalto;
    // Creamos una variable para controlar si está en el piso.
    public bool enSuelo;
    //Creamos un objeto para asignárselo en Unity dicha referencia
    public Transform refPie;
    //Velocidad del personaje;
    public float velX = 10;
    // Start is called before the first frame update

    void Start()
    {
        //Lo cargamos al iniciar
        anim = GetComponent<Animator>();
        //Lo cargamos al iniciar
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        float movX;
        //Preguntamos por los ejes, en vez de por las teclas, para poder manejarlo a la vez con el mando.
        movX = Input.GetAxis("Horizontal");
        anim.SetFloat("absMovX", Mathf.Abs(movX));
        rb.velocity = new Vector2(velX * movX, rb.velocity.y);
        //Para saber si el pie está tocando el suelo, le decimos que la referencia del pie que le hemos asignado en Unity está a esa distancia y en la capa 8 para que sea true
        enSuelo = Physics2D.OverlapCircle(refPie.position, 1f, 1 << 8);
        anim.SetBool("enSuelo", enSuelo);

        //Preguntamos si le hemos dado al botón saltar o a la tecla space que tiene asignado por sistema y le dará el impulso para saltar.
        if (Input.GetButtonDown("Jump") && enSuelo)
        {
            rb.AddForce(new Vector2(0, fuerzaSalto), ForceMode2D.Impulse);
        }
        //Para ir a la izda.
        if (movX < 0) transform.localScale = new Vector3(-1, 1, 1);
        //Para ir a la drcha
        if (movX > 0) transform.localScale = new Vector3(1, 1, 1);


    }

    //Para llamarlo siempre a 50 frames por segundo
    private void FixedUpdate()
    {
        //Movimiento de camara
        Vector3 dondeEstoy = Camera.main.transform.position;
        Vector3 dondeQuieroIr = transform.position - new Vector3(0, 0, 20);
        //La posición de la cámara es igual a el lugar donde estoy mas la diferencia entre las dos posiciones por el 5%
        //Camera.main.transform.position = dondeEstoy + (dondeQuieroIr - dondeEstoy) * 0.05f;
        //Esta es la forma correcta de hacerlo, Lerp se encarga de calcular con los tres elementos
        Camera.main.transform.position = Vector3.Lerp(dondeEstoy, dondeQuieroIr, .05f);
    }
}
