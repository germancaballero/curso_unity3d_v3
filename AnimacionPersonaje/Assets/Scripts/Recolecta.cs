﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recolecta : MonoBehaviour
{
    //Administrador del juego
    #region Campos
    //Creamos la lista de panes
    List<GameObject> panes = new List<GameObject>();
    //Variable del prefabs pan
    [SerializeField] GameObject panPrefab;

    #endregion

    #region Propiedades
    //Devuelve el siguiente pan a recoger
    public GameObject RecogePan
    {
        get
        {
            if(panes.Count > 0)
            {
            //devuelve el primer elemento
            return panes[0];
            }
            else
            {
                return null;
            }
           
        }
    }
    #endregion
    #region Métodos



    // Update is called once per frame
    void Update()
    {
        //Agregar un pan boton derecho del ratón(1) el izquierdo es (0)
        if (Input.GetMouseButtonDown(1))
        {
            //Calcular la posición del mundo de donde está el mouse
            Vector3 posMouse = Input.mousePosition;
            posMouse.z = -Camera.main.transform.position.z;
            Vector3 posMundo = Camera.main.ScreenToWorldPoint(posMouse);

            //Crear el pan de forma dinámica
            GameObject panecillo = Instantiate<GameObject>(panPrefab);
            //Le asignamos una posición al pan
            panecillo.transform.position = posMundo;
            //Lo agregamos a la lista
            panes.Add(panecillo);
        }
    }
    //Quita el pan de la lista
    //"panAQuitar" el pan que se va a quitar de la lista
    public void QuitaPan(GameObject panAQuitar)
    {
        panes.Remove(panAQuitar);
        Destroy(panAQuitar);
    }
    #endregion
}
