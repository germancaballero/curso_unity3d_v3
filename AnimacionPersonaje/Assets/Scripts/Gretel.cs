﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gretel : MonoBehaviour
{
    #region Campos

    bool recolectando = false;

    //Para recolectar
    new Rigidbody2D rigidbody2D;
    Recolecta elRecolector;
    GameObject panARecoger;
    const float FuerzaEmpuje = 2.0f;

    #endregion

    #region Métodos
    // Start is called before the first frame update
    void Start()
    {
        //Centramos el personaje para el inicio del juego.
        Vector3 pos = transform.position;
        pos.x = 0;
        pos.y = 0;
        pos.z = 0;
        transform.position = pos;
        //Inicializar objetos de recolectar
        rigidbody2D = GetComponent<Rigidbody2D>();
        elRecolector = Camera.main.GetComponent<Recolecta>();
    }

    // Se ejecuta cuando se hace clic sobre el muñeco
    void OnMouseDown()
    {
        //Ignorar si la niña está recolectando
        if (!recolectando)
        {
            //Llama a la clase para coger otro pan 
            VePorOtroPan();
        }
    }
    //Cuando choca con pan se ejecuta
    private void OnTriggerStay2D(Collider2D collision)
    {
        //solo responde si choca con el pan que quiero recoger
        if (collision.gameObject == panARecoger)
        {
            //Quitar el pan de la lista y avanzo al siguiente
            elRecolector.QuitaPan(panARecoger);
            //Parar al personaje
            rigidbody2D.velocity = Vector2.zero;
            VePorOtroPan();
        }
    }

    private void VePorOtroPan()
    {
        //Calcula la dirección hacia el pan y se mueve hacia allá
        panARecoger = elRecolector.RecogePan;
        //Verifico que no haya recogido todos los panes
        if (panARecoger != null)
        {
        Vector2 direccion = new Vector2(panARecoger.transform.position.x - transform.position.x, panARecoger.transform.position.y - transform.position.y);
    //Avanzado uniformemente
    direccion.Normalize();
            rigidbody2D.AddForce(direccion* FuerzaEmpuje, ForceMode2D.Impulse);
            //Decirle que está recolectando
            recolectando = true;
        }
   else
        {
            recolectando = false;
        }
    }
            

    #endregion
}
