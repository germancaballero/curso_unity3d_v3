﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _17ArraysParalelos
{
    //Desarrollar un programa que permita cargar 5 nombres de personas y sus edades respectivas. Luego de realizar la carga por teclado de todos los datos imprimir los nombres de las personas mayores de edad (mayores o iguales a 18 años)
    class ArraysParalelos
    {
        //Definimos dos arrays
        private string[] nombres;
        private int[] edades;

        //Creación del método Cargar que recoge los datos del teclado
        public void Cargar()
        {
            //Creamos los arrays de un tamaño de 5 cada uno
            nombres = new string[5];
            edades = new int[5];
            //Utilizamos el for para preguntar por los datos y cargas los arrays
            for (int f = 0; f < nombres.Length; f++)
            {
                Console.Write("Ingrese nombre:");
                nombres[f] = Console.ReadLine();
                Console.Write("Ingrese edad:");
                string linea;
                linea = Console.ReadLine();
                edades[f] = int.Parse(linea);
            }
        }

        //Creamos el método para saber si es mayor de edad y mostrarlas en pantalla.
        public void MayoresEdad()
        {
            Console.WriteLine("Personas mayores de edad.");
            for (int f = 0; f < nombres.Length; f++)
            {
                if (edades[f] >= 18)
                {
                    Console.WriteLine(nombres[f]);
                }
            }
            Console.ReadKey();
        }

        //Inicio del programa
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase para poder llamar y ejecutar los métodos
            ArraysParalelos pv = new ArraysParalelos();
            //Llamamos a Cargar
            pv.Cargar();
            //Llamamos a MayoresEdad
            pv.MayoresEdad();
        }
    }
}
