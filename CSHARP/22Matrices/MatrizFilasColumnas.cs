﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22Matrices
{
    //Crear una matriz de n * m filas (cargar n y m por teclado) Imprimir la matriz completa y la última fila.
    class MatrizFilasColumnas
    {
        //Para definir una matriz debemos disponer una coma dentro de los corchetes
        private int[,] mat;

        //Creación del método Cargar que recoge los datos del teclado
        public void Cargar()
        {
            //Pedimos datos para crear la matriz
            Console.Write("Cuantas fila tiene la matriz:");
            string linea;
            linea = Console.ReadLine();
            int filas = int.Parse(linea);
            Console.Write("Cuantas columnas tiene la matriz:");
            linea = Console.ReadLine();
            int columnas = int.Parse(linea);
            //Creamos la matriz dando primero las filas y luego las columnas
            mat = new int[filas, columnas];
            //Se utiliza un for anidado, el primer for que incrementa el contador f lo utilizamos para recorrer las filas y el contador interno llamado c lo utilizamos para recorrer las columnas.
            for (int f = 0; f < mat.GetLength(0); f++)
            {
                for (int c = 0; c < mat.GetLength(1); c++)
                {
                    Console.Write("Ingrese componente:");
                    linea = Console.ReadLine();
                    mat[f, c] = int.Parse(linea);
                }
            }
        }

        //Creamos método imprimir
        public void Imprimir()
        {
            // Usamos for anidados para leer la matriz y poder mostrarla, el de fuera las filas
            for (int f = 0; f < mat.GetLength(0); f++)
            {
                //Este for para leer las columnas
                for (int c = 0; c < mat.GetLength(1); c++)
                {
                    Console.Write(mat[f, c] + " ");
                }
                Console.WriteLine();
            }
        }

        //Método para imprimir la ultima fila
        public void ImprimirUltimaFila()
        {
            Console.WriteLine("Ultima fila");
            //Un solo for porque tenemos la fila que es, pero hay que recorrer todas las columnas para mostrarlas
            for (int c = 0; c < mat.GetLength(1); c++)
            {
                Console.Write(mat[mat.GetLength(0) - 1, c] + " ");
            }
        }

        //Inicio del programa
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase para poder llamar y ejecutar los métodos
            MatrizFilasColumnas ma = new MatrizFilasColumnas();
            ma.Cargar();
            ma.Imprimir();
            ma.ImprimirUltimaFila();
            Console.ReadKey();
        }
    }
}

/*  
     Crear una matriz de n * m filas (cargar n y m por teclado) Imprimir el mayor elemento y la fila y columna donde se almacena.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22Matrices
{
    class MatrizFilasColumnas
    {
        private int[,] mat;

        public void Cargar() 
        {
            Console.Write("Cuantas fila tiene la matriz:");
            string linea;
            linea=Console.ReadLine();
            int filas=int.Parse(linea);
            Console.Write("Cuantas columnas tiene la matriz:");
            linea=Console.ReadLine();
            int columnas=int.Parse(linea);
            mat=new int[filas,columnas];
            for(int f = 0; f < mat.GetLength(0); f++)
            {
                for(int c = 0; c <mat.GetLength(1); c++)
                {
                    Console.Write("Ingrese componente:");
                    linea = Console.ReadLine();
                    mat[f,c]=int.Parse(linea);
                }
            }
        }

        public void ImprimirMayor() 
        {
    	    int mayor=mat[0,0];
    	    int filamay=0;
    	    int columnamay=0;
            for(int f = 0; f < mat.GetLength(0); f++)
            {
                for(int c = 0; c < mat.GetLength(1); c++) 
                {
                    if (mat[f,c] > mayor) 
                    {
                        mayor=mat[f,c];
                        filamay=f+1;
                        columnamay=c+1;
                    }
                }
            }
            Console.WriteLine("El elemento mayor es:"+mayor);
            Console.WriteLine("Se encuentra en la fila:"+filamay+ " y en la columna: "+columnamay);
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            MatrizFilasColumnas ma = new MatrizFilasColumnas();
            ma.Cargar();
            ma.ImprimirMayor();
        }
    }
}



    Crear una matriz de n * m filas (cargar n y m por teclado) Intercambiar la primer fila con la segunda. Imprimir luego la matriz. 


    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22Matrices
{
    class MatrizFilasColumnas
    {
        private int[,] mat;

        public void Cargar() 
        {
            Console.Write("Cuantas fila tiene la matriz:");
            string linea;
            linea=Console.ReadLine();
            int filas=int.Parse(linea);
            Console.Write("Cuantas columnas tiene la matriz:");
            linea=Console.ReadLine();
            int columnas=int.Parse(linea);
            mat=new int[filas,columnas];
            for(int f = 0; f < mat.GetLength(0); f++)
            {
                for(int c = 0;c < mat.GetLength(1); c++)
                {
                    Console.Write("Ingrese componente:");
                    linea = Console.ReadLine();
                    mat[f,c]=int.Parse(linea);
                }
            }
        }

        public void Intercambiar()
        {
            for (int c = 0; c < mat.GetLength(1); c++)
            {
                int aux = mat[0,c];
                mat[0,c] = mat[1,c];
                mat[1,c] = aux;
            }
        }

        public void Imprimir() 
        {
            for(int f = 0; f < mat.GetLength(0); f++)
            {
                for(int c = 0; c < mat.GetLength(1); c++)
                {
                    Console.Write(mat[f,c]+" ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
   
        static void Main(string[] args)
        {
            MatrizFilasColumnas ma = new MatrizFilasColumnas();
            ma.Cargar();
            ma.Intercambiar();
            ma.Imprimir();
        }
    }
}



    Crear una matriz de n * m filas (cargar n y m por teclado) Imprimir los cuatro valores que se encuentran en los vértices de la misma (mat[0][0] etc.) 




    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22Matrices
{
    class MatrizFilasColumnas
    {
        private int[,] mat;

        public void Cargar() 
        {
            Console.Write("Cuantas fila tiene la matriz:");
            string linea;
            linea=Console.ReadLine();
            int filas=int.Parse(linea);
            Console.Write("Cuantas columnas tiene la matriz:");
            linea=Console.ReadLine();
            int columnas=int.Parse(linea);
            mat=new int[filas,columnas];
            for(int f = 0; f < mat.GetLength(0); f++)
            {
                for(int c = 0; c < mat.GetLength(1); c++)
                {
                    Console.Write("Ingrese componente:");
                    linea = Console.ReadLine();
                    mat[f,c]=int.Parse(linea);
                }
            }
        }

        public void ImprimirVertices() 
        {
    	    Console.WriteLine("Vértice superior izquierdo:");
            Console.WriteLine(mat[0,0]);
    	    Console.WriteLine("Vértice superior derecho:");        
            Console.WriteLine(mat[0,mat.GetLength(1)-1]);
    	    Console.WriteLine("Vértice inferior izquierdo:");        
            Console.WriteLine(mat[mat.GetLength(0)-1,0]);
    	    Console.WriteLine("Vértice inferior derecho:");        
            Console.WriteLine(mat[mat.GetLength(0)-1,mat.GetLength(1)-1]);
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            MatrizFilasColumnas ma = new MatrizFilasColumnas();
            ma.Cargar();
            ma.ImprimirVertices();
        }
    }
}
     
}*/