﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulo
{
    // Pedir tres lados, devolver el lado mayor si lo hay, sino decir que es equilatero. Clases: 1.- Pedir datos desde el Main 2.- Lado mayor 3.- Si es Equilatero.
    //Main es la encargada de mostrar los datos.
    class Triangulo
    {
        //Declaramos las variables externas y públicas para usarlas en toda la clase.
        public int valor1, valor2, valor3;
        public string mayor, equi;
        static void Main(string[] args)
        {      
            //Nos creamos el objeto.
            Triangulo Tri = new Triangulo();
            //Llamamos a cargar los valores.
            Tri.cargarValores();
            //Preguntamos por el lado mayor, llamamos a la clase y le enviamos los valores, hay que ponerle el objeto al que pertenece (Tri.) porque estamos en el Main y no tiene retorno de datos.
            Tri.mayor = Tri.ladoMayor(Tri.valor1, Tri.valor2, Tri.valor3);
            //Preguntamos si es equilatero, llamamos a la clase y le enviamos los valores, hay que ponerle el objeto al que pertenece (Tri.)porque estamos en el Main y no tiene retorno de datos.
            Tri.equi = Tri.Equilatero(Tri.valor1, Tri.valor2, Tri.valor3);
            //Mostramos el lado mayor.
            Console.WriteLine("El lado mayor vale: " + Tri.mayor);
            Console.WriteLine(Tri.equi);
            Console.ReadKey();

        }

        public string ladoMayor(int valor1, int valor2, int valor3)
        {
            //Declaramos variable.
            string devolver;
            //Preguntamos
            if (valor1 > valor2 && valor1 > valor3)
            {
                //devolver = valor1.toString()
                devolver = "" + valor1;
            }
            else
            {
                if (valor2 > valor3)
                {
                    devolver = "" + valor2;
                }
                else
                {
                    devolver = "" + valor3;
                }

            }
            //Y el dato válido, es el que devolvemos y mostraremos como mayor.
            return devolver;
        }
        //Comprobamos si es equilatero.
        public string Equilatero(int valor1, int valor2, int valor3)
        {
            
            if (valor1 == valor2 && valor1 == valor3)
            {
               equi = "Es un triángulo equilátero";
            }
            else
            {
                equi = "No es un triángulo equilátero";
            }
            return equi;
        }
        //Cargamos los valores
        public void cargarValores()
        {
            //Declaramos variable
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            //declaramos la variable y a la vez le introducimos el valor convertido.
            valor1 = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            valor2 = int.Parse(linea);
            Console.Write("Ingrese tercer valor:");
            linea = Console.ReadLine();
            valor3 = int.Parse(linea);
        }


    }
}
