﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18ArraysMayorMenor
{
    //Confeccionar un programa que permita cargar los nombres de 5 operarios y sus sueldos respectivos. Mostrar el sueldo mayor y el nombre del operario.
    class ArraysMayorMenor
    {
        //Definimos dos arrays
        private string[] nombres;
        private float[] sueldos;

        //Creación del método Cargar que recoge los datos del teclado
        public void Cargar()
        {
            //Creamos los arrays de un tamaño de 5 cada uno
            nombres = new string[5];
            sueldos = new float[5];
            //Utilizamos el for para preguntar por los datos y cargas los arrays
            for (int f = 0; f < nombres.Length; f++)
            {
                Console.Write("Ingrese el nombre del empleado:");
                nombres[f] = Console.ReadLine();
                Console.Write("Ingrese el sueldo:");
                string linea;
                linea = Console.ReadLine();
                sueldos[f] = float.Parse(linea);
            }
        }

        //Creamos el método para saber si el mayor sueldo y mostrar el nombre en pantalla.
        public void MayorSueldo()
        {
            //Creamos dos variables para poder comparar los datos
            float mayor;
            int pos;
            //Inicializamos la variable mayor con el dato del primer dato del array
            mayor = sueldos[0];
            //Inicializamos en 0 para que no de error al comparar y no esté en null
            pos = 0;
            //Con el for recorremos el array de sueldos preguntando si es mayor del que tenemos guardado en pos y si es mayor, lo cambiamos, cuando termine el for ya tenemos el sueldo mayor. En pos solo guadamos el índice, no el valor.
            for (int f = 1; f < nombres.Length; f++)
            {
                if (sueldos[f] > mayor)
                {
                    mayor = sueldos[f];
                    pos = f;
                }
            }
            //Escribimos el nombre del empleado
            Console.WriteLine("El empleado con sueldo mayor es " + nombres[pos]);
            //Escribimos el sueldo
            Console.WriteLine("Tiene un sueldo:" + mayor);
            Console.ReadKey();
        }

        //Inicio del programa
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase para poder llamar y ejecutar los métodos
            ArraysMayorMenor pv = new ArraysMayorMenor();
            //Llamamos a Cargar
            pv.Cargar();
            //Llamamos a MayorSueldo
            pv.MayorSueldo();
        }
    }
    /* Cargar un vector de n elementos. imprimir el menor y un mensaje si se repite dentro del vector. 
     * 
     * using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18ArraysMayorMenor
{
    class ArraysMayorMenor
    {
        private int[] vec;
        private int menor;

        public void Cargar() 
        {
            Console.Write("Cuantos elementos desea cargar:");
            string linea;
            linea = Console.ReadLine();
            int n=int.Parse(linea);
            vec=new int[n];
            for(int f=0;f < vec.Length;f++) 
            {
                Console.Write("Ingrese componente:");
                linea = Console.ReadLine();
                vec[f]=int.Parse(linea);
            }
        }

        public void MenorElemento() 
        {
            menor=vec[0];
            for(int f=1;f < vec.Length;f++) 
            {
                if (vec[f] < menor) 
                {
                    menor=vec[f];
                }
            }
            Console.WriteLine("El elemento menor es:" + menor);        
        }

        public void RepiteMenor() 
        {
            int cant=0;
            for(int f=0;f < vec.Length;f++) 
            {
                if (vec[f]==menor) 
                {
                    cant++;
                }
            }
            if (cant > 1) 
            {
                Console.WriteLine("Se repite el menor en el vector.");    
            }
            else 
            {
                Console.WriteLine("No se repite el menor en el vector.");
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            ArraysMayorMenor pv = new ArraysMayorMenor();
            pv.Cargar();
            pv.MenorElemento();
            pv.RepiteMenor();
        }
    }
}
     * 
     */
}
