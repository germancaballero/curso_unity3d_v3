﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace _32ListaCola
{
    class Cola
    {
        //Declaramos el nodo
        class Nodo
        {
            public int info;
            public Nodo sig;
        }

        //creamos las variables para el inicio y el final de la lista
        private Nodo raiz, fondo;

        //Inicializamos el constructor de la lista
        public Cola()
        {
            raiz = null;
            fondo = null;
        }

        //Método para saber si la lista está vacía o no
        public bool Vacia()
        {
            //Si está vacía
            if (raiz == null)
                return true;
            else
                return false;
        }

        //Insertamos en la lista un nodo
        public void Insertar(int info)
        {
            //Inicializamos el objeto tipo nodo
            Nodo nuevo;
            //Creamos el objeto tipo nodo
            nuevo = new Nodo();
            //Insertamos la información en el nodo nuevo
            nuevo.info = info;
            //como es el último insertado, no apunta a ningún otro nodo y se le asigna null
            nuevo.sig = null;
            //si está vacía entramos
            if (Vacia())
            {
                //si la lista está vacía el nodo nuevo es el raiz y el fondo.
                raiz = nuevo;
                fondo = nuevo;
            }
            else
            {
                //si ya hay algún nodo le asignamos la dirección al último de la lista
                fondo.sig = nuevo;
                fondo = nuevo;
            }
        }

        //Para Extraer un nodo
        public int Extraer()
        {
            //Si no está vacía entramos
            if (!Vacia())
            {
                //Cogemos la información del nodo.
                int informacion = raiz.info;
                //Si el de inicio es igual que el último hay un solo nodo
                if (raiz == fondo)
                {
                    //Le asignamos las direcciones a null al extraer el último de la lista y se va a destruir
                    raiz = null;
                    fondo = null;
                }
                //Si hay más de una se le asigna a la anterior la dirección de la primera que se queda.
                else
                {
                    raiz = raiz.sig;
                }
                //devolvemos la información del nodo
                return informacion;
            }
            else
                //está vacía
                return int.MaxValue;
        }

        //Mostrar la lista
        public void Imprimir()
        {
            //Creamos el nodo y lo cargamos con el raíz
            Nodo reco = raiz;
            Console.WriteLine("Listado de todos los elementos de la cola.");
            //recorremos la lista mientras haya nodos
            while (reco != null)
            {
                //Escribes la información
                Console.Write(reco.info + "-");
                //Asignamos la dirección del que se borra para asignar al que queda en la cola.
                reco = reco.sig;
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {

            Cola cola1 = new Cola();
            cola1.Insertar(5);
            cola1.Insertar(10);
            cola1.Insertar(50);
            cola1.Imprimir();
            Console.WriteLine("Extraemos uno de la cola:" + cola1.Extraer());
            cola1.Imprimir();
            Console.ReadKey();
        }
    }
}
