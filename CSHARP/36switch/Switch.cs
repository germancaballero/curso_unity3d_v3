﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//el switch sustituye los if encadenados
/*Estructura del switch:
 switch(variable) {
    case valor1:
         Instrucciones
         break;
    case valor2:
         Instrucciones
         break;
    default:
         Instrucciones
         break;
}
*/
namespace _36switch
{
    //Ingresar un valor entero entre 1 y 5. Luego mostrar en castellano el valor ingresado. Si se ingresa un valor fuera de dicho rango mostrar un mensaje indicando tal situación
    class Switch
    {
        static void Main(string[] args)
        {
            //Pedimos el dato al usuario
            Console.Write("Ingrese un valor entre 1 y 5:");
            //Declaramos la variable que pondremos en el switch
            int valor = int.Parse(Console.ReadLine());
            //Iniciamos el Switch
            switch (valor)
            {
                //se mira el valor, si es, entra
                case 1:
                    //Imprime el valor
                    Console.Write("uno");
                    //Paramos y terminamos el switch 
                    break;
                case 2:
                    Console.Write("dos");
                    break;
                case 3:
                    Console.Write("tres");
                    break;
                case 4:
                    Console.Write("cuatro");
                    break;
                case 5:
                    Console.Write("cinco");
                    break;
                //en caso de que no coincida ningún valor se ejecuta el default.
                default:
                    Console.Write("Se ingreso un valor fuera de rango");
                    break;
            }
            Console.ReadKey();
        }
    }

    /*
     Ingresar un número entre uno y cinco en castellano. Luego mostrar en formato numérico. Si se ingresa un valor fuera de dicho rango mostrar un mensaje indicando tal situación

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Estructuraswitch2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ingrese un número en castellano entre uno y cinco:");
            string nro = Console.ReadLine();
            switch (nro)
            {
                case "uno": Console.Write(1);
                            break;
                case "dos": Console.Write(2);
                            break;
                case "tres": Console.Write(3);
                            break;
                case "cuatro": Console.Write(4);
                            break;
                case "cinco": Console.Write(5);
                            break;
                default: Console.Write("Debe ingresar un valor entre uno y cinco");
                            break;
            }
            Console.ReadKey();
        }
    }
}
     */
}
