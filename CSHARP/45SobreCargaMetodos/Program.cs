﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _45SobreCargaMetodos
{/*
    class Program
    {
        public int Sumar(int x1, int x2)
        {
            int s = x1 + x2;
            return s;
        }
        public string Sumar(string s1, string s2)
        {
            string s = s1 + s2;
            return s;
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            Console.WriteLine("La suma de 5 + 10 es: " + p.Sumar(5, 10));
            Console.WriteLine("La concatenación de \"Juan\" y \"Carlos\" es " + p.Sumar("Juan", "Carlos"));
            Console.ReadKey();
        }
    }

     Plantear una clase llamada Ventana que defina dos métodos sobrecargados que muestren un mensaje en la consola.
    El primero lo muestrar donde se encuentra actualmente el cursor. 
    SetCursorPosition()

    El segundo lo muestra en una determinada columna y fila. 
    SetCursorPosition(,)

        El tercero lo muestra en una determinada columna,fila y con un color de letra.
    
    Y por último similar al anterior más un color de fondo. 
     */
    class Ventana
    {

        static void Main(string[] args)
        {
            Ventana v = new Ventana();
            v.Mostrar("Hola mundo");
            v.Mostrar("Holi mundo", 30, 10);
            v.Mostrar("HOLA MUNDO", 30, 12, ConsoleColor.Red);
            v.Mostrar("holi mundo", 30, 14, ConsoleColor.Red, ConsoleColor.Blue);
            Console.ReadKey();
        }
        public void Mostrar(string mensaje)
        {
            Console.Write(mensaje);
        }
        public void Mostrar(string mensaje, int columna, int fila)
        {
            Console.SetCursorPosition(columna, fila);
            Console.Write(mensaje);
        }
        public void Mostrar(string mensaje, int columna, int fila, ConsoleColor colorletra)
        {
            Console.ForegroundColor = colorletra;
            Console.Write(mensaje, columna, fila);
        }
        public void Mostrar(string mensaje, int columna, int fila, ConsoleColor colorletra, ConsoleColor colorfondo)
        {
            Console.BackgroundColor = colorfondo;
            Console.Write(mensaje, columna, fila, colorletra);
        }

    }
}

