﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 try {
        [instrucciones 1]
     } 
catch([excepción 1]) 
    {
        [instrucciones 2]
    } 
finally 
    {
        [instrucciones 3]
    }
 */
//El objetivo de este bloque es liberar recursos que se solicitan en el bloque try. El bloque finally se ejecuta siempre, inclusive si se genera la captura de una excepción.
//Tener en cuenta que si no se disparan ninguna excepción en un bloque try luego de esto se ejecuta el bloque 'finally'.
//El bloque finally es opcional y en el caso de estar presente se coloca después del último bloque catch.
namespace _41Finally
{
    //Crear un archivo de texto con dos líneas. Luego proceder a leer el contenido del archivo de texto y mostrarlo por pantalla. Asegurarse de liberar el archivo luego de trabajar con el mismo
    class Finally
    {
        static void Main(string[] args)
        {
            StreamWriter txt = null;
            try
            {
                txt = new StreamWriter("datos.txt");
                txt.WriteLine("Línea 1");
                txt.WriteLine("Línea 2");
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (txt != null)
                    txt.Close();
            }

            StreamReader txtleer = null;
            try
            {
                txtleer = new StreamReader("datos.txt");
                string linea = txtleer.ReadLine();
                while (linea != null)
                {
                    Console.WriteLine(linea);
                    linea = txtleer.ReadLine();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (txtleer != null)
                    txtleer.Close();
            }
            Console.ReadKey();
        }
    }
}
