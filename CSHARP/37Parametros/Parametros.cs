﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _37Parametros
{
    //Implementar un método que reciba dos enteros y luego imprima de uno en uno desde el valor menor hasta el valor mayor.
    class Parametros
    {
        //En el método recibimos dos parámetros de tipo entero
        public void MostrarRango(int menor, int mayor)
        {
            for (var x = menor; x <= mayor; x++)
            {
                Console.Write(x + " ");
            }
        }

        static void Main(string[] args)
        {
            Parametros p = new Parametros();
            //llamamoa al método enviándole dos parámetros.
            p.MostrarRango(1, 25);
            Console.ReadKey();
        }
    }

    /*Ejemplo de parametros por nombre
   using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParametrosNombre1
{
    class Program
    {
    static void Main(string[] args)
    {
            Program ob1 = new Program();
        ob1.Imprimir(fila: 12, columna: 40, mensaje: "Hola Mundo");
        ob1.Imprimir(mensaje: "Fin", fila: 13, columna: 5);
        Console.ReadKey();
    }

    public void Imprimir(string mensaje, int columna, int fila)
    {
        Console.SetCursorPosition(columna, fila);
        Console.WriteLine(mensaje);
    }
    }
    
}
     */



    /*Confeccionar un método que reciba como parámetros tres valores enteros y retorne el mayor de los mismos.
     
     using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParametrosValor2
{
    class Program
    {
        public int Mayor(int v1, int v2, int v3)
        {
            if (v1 >= v2 && v1 >= v3)
            {
                return v1;
            }
            else
            {
                if (v2 >= v3)
                {
                    return v2;
                }
                else
                {
                    return v3;
                }
            }
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            Console.Write("Ingrese primer valor:");
            int x1 = int.Parse(Console.ReadLine());
            Console.Write("Ingrese segundo valor:");
            int x2 = int.Parse(Console.ReadLine());
            Console.Write("Ingrese tercer valor:");
            int x3 = int.Parse(Console.ReadLine());
            Console.Write("El mayor valor de los tres es:" + p.Mayor(x1, x2, x3));
            Console.ReadKey();
        }
    }
}



    Confeccionar un método que reciba un entero entre 1 y 10 y retorne el valor en castellano. 


    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParametrosValor3
{
    class Program
    {
        public string RetornarCastellano(int x)
        {
            string valor;
            switch (x)
            {
                case 1: valor = "uno";
                    break;
                case 2: valor = "dos";
                    break;
                case 3: valor = "tres";
                    break;
                case 4: valor = "cuatro";
                    break;
                case 5: valor = "cinco";
                    break;
                case 6: valor = "seis";
                    break;
                case 7: valor = "siete";
                    break;
                case 8: valor = "ocho";
                    break;
                case 9: valor = "nueve";
                    break;
                case 10: valor = "diez";
                    break;
                default: valor = "valor fuera de rango";
                    break;
            }
            return valor;
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            Console.Write("Ingrese un valor entre 1 y 10:");
            int x = int.Parse(Console.ReadLine());
            Console.Write(p.RetornarCastellano(x));
            Console.ReadKey();
        }
    }
}

     */

}
