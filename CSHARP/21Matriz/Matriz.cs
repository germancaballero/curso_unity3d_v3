﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21Matriz
{
    //Crear una matriz de 3 filas por 5 columnas con elementos de tipo int, cargar sus componentes y luego imprimirlas.

    class Matriz
    {
        //Para definir una matriz debemos disponer una coma dentro de los corchetes
        private int[,] mat;

        //Creación del método Cargar que recoge los datos del teclado
        public void Cargar()
        {
            //Creamos la matriz dando primero las filas y luego las columnas
            mat = new int[3, 5];
            //Se utiliza un for anidado, el primer for que incrementa el contador f lo utilizamos para recorrer las filas y el contador interno llamado c lo utilizamos para recorrer las columnas.
            for (int f = 0; f < 3; f++)
            {
                for (int c = 0; c < 5; c++)
                {
                    Console.Write("Ingrese componente:");
                    string linea;
                    linea = Console.ReadLine();
                    mat[f, c] = int.Parse(linea);
                }
            }
        }

        //Creamos método imprimir
        public void Imprimir()
        {
            // Usamos for anidados para leer la matriz y poder mostrarla
            for (int f = 0; f < 3; f++)
            {
                for (int c = 0; c < 5; c++)
                {
                    Console.Write(mat[f, c] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }


        //Inicio del programa
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase para poder llamar y ejecutar los métodos
            Matriz ma = new Matriz();
            //Llamamos a Cargar
            ma.Cargar();
            //Llamamos a Imprimir
            ma.Imprimir();
        }
    }
   
     Crear y cargar una matriz de 4 filas por 4 columnas. Imprimir la diagonal principal.

              x    -    -    -
              -    x    -    -
              -    -    x    -
              -    -    -    x

 */
    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21Matriz
{
    class Matriz
    {
        private int[,] mat;
        public void Cargar() 
        {
            mat=new int[4,4];

            
            for(int f = 0; f < 4; f++) 
            {
                for(int c = 0; c<4; c++) 
                {
                    Console.Write("Ingrese componente:");
                    string linea;
                    linea = Console.ReadLine();
                    mat[f, c] = int.Parse(linea);

                }
            }
        }

        public void ImprimirDiagonalPrincipal() 
        {
            for(int k = 0; k < 4; k++) 
            {
                Console.Write(mat[k,k]+" ");
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Matriz ma = new Matriz();
            ma.Cargar();
            ma.ImprimirDiagonalPrincipal();
        }
    }
}
/*



    Crear y cargar una matriz de 3 filas por 4 columnas. Imprimir la primer fila. Imprimir la última fila e imprimir la primer columna.


    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21Matriz
{
    class Matriz
    {
        private int[,] mat;

        public void Cargar() 
        {
            mat=new int[3,4];
            for(int f = 0; f < 3; f++) 
            {
                for(int c = 0; c < 4; c++) 
                {
                    Console.Write("Ingrese componente:");
                    string linea;
                    linea = Console.ReadLine();
                    mat[f,c]=int.Parse(linea);
                }
            }
        }

        public void PrimerFila() 
        {
    	    Console.WriteLine("Primer fila de la matriz:");
            for(int c = 0; c < 4; c++) 
            {
                Console.WriteLine(mat[0,c]);
            }
        }

        public void UltimaFila() 
        {
    	    Console.WriteLine("Ultima fila de la matriz:");
            for(int c = 0; c < 4; c++) 
            {
                Console.WriteLine(mat[2,c]);
            }
        }

        public void PrimerColumna() 
        {
    	    Console.WriteLine("Primer columna:");
            for(int f = 0; f < 3; f++) 
            {
                Console.WriteLine(mat[f,0]);
            }
        }

        static void Main(string[] args)
        {
            Matriz ma = new Matriz();
            ma.Cargar();
            ma.PrimerFila();
            ma.UltimaFila();
            ma.PrimerColumna();
            Console.ReadKey();
        }
    }
}




    Crear una matriz de 2 filas y 5 columnas. Realizar la carga de componentes por columna (es decir primero ingresar toda la primer columna, luego la segunda columna y así sucesivamente)
Imprimir luego la matriz. 


    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21Matriz
{
    class Matriz
    {
        private int[,] mat;

        public void Cargar() 
        {
            mat=new int[2,5];
            Console.WriteLine("Carga de la matriz por columna:");
            for(int c = 0; c < 5; c++) 
            {        
                for(int f = 0; f < 2; f++) 
                {
                    Console.Write("Ingrese componente " + " de la fila " + f + " y la columna "+ c + " :");
                    string linea;
                    linea = Console.ReadLine();
                    mat[f,c]=int.Parse(linea);
                }
            }
        }

        public void Imprimir() 
        {
            for(int f = 0; f < 2; f++) 
            {
                for(int c = 0; c < 5; c++) 
                {
                    Console.Write(mat[f,c]+" ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Matriz ma = new Matriz();
            ma.Cargar();
            ma.Imprimir();
        }
    }
}
     
}*/
