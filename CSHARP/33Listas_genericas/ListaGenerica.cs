﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace _33Listas_genericas
{
    /*Continuando con el tema de listas trabajaremos con las listas genéricas. Una lista se comporta como genérica cuando las inserciones y extracciones se realizan en cualquier parte de la lista.
        Codificaremos una serie de métodos para administrar listas genéricas.
     */
    class ListaGenerica
    {
        class Nodo
        {
            public int info;
            public Nodo sig;
        }

        private Nodo raiz;
        
        //Constructor
        public ListaGenerica()
        {
            raiz = null;
        }

        //Insertar pos y valor
        void Insertar(int pos, int x)
        {
            //verificamos que exista esa posición en la lista
            if (pos <= Cantidad() + 1)
            {
                //Creamos Nodo
                Nodo nuevo = new Nodo();
                //Insertamos el valor
                nuevo.info = x;
                //Para saber si se inserta al principio de la lista preguntamos si en pos llega un 1.
                if (pos == 1)
                {
                    //Si llega un 1 luego enlazamos el puntero sig del nodo que creamos con la dirección del primer nodo de la lista y luego desplazamos raiz al nodo que acabamos de crear
                    nuevo.sig = raiz;
                    raiz = nuevo;
                }
                else
                //Si no se inserta al principio de la lista preguntamos si se inserta al final
                    if (pos == Cantidad() + 1)
                    {
                        //En caso de insertarse al final recorremos la lista hasta el último nodo
                        Nodo reco = raiz;
                        while (reco.sig != null)
                        {
                        reco = reco.sig;
                        }
                        //enlazamos el puntero sig del último nodo de la lista con la dirección del nodo que acabamos de crear
                        reco.sig = nuevo;
                        nuevo.sig = null;
                    }
                    else
                    {
                    Nodo reco = raiz;
                    //for donde avanzamos un puntero auxiliar y nos detenemos una posición antes a donde tenemos que insertarlo
                    for (int f = 1; f <= pos - 2; f++)
                        reco = reco.sig;

                    //Disponemos otro puntero auxiliar que apunte al nodo próximo a donde está apuntando reco. Ahora enlazamos el puntero sig del nodo apuntado por reco con la dirección del nodo creado y el puntero sig del nodo creado con la dirección del nodo siguiente
                    Nodo siguiente = reco.sig;
                    reco.sig = nuevo;
                    nuevo.sig = siguiente;
                }
            }
        }
        //El método extraer recibe como parámetro la posición del nodo a extraer
        public int Extraer(int pos)
        {
            //Primero verificamos que la posición exista en la lista
            if (pos <= Cantidad())
            {
                int informacion;
                //verificamos si el nodo a extraer es el primero de la lista
                if (pos == 1)
                {
                    //Si es el primero guardamos en una variable auxiliar la información del nodo y avanzamos el puntero raiz
                    informacion = raiz.info;
                    raiz = raiz.sig;
                }
                else
                {
                    Nodo reco;
                    reco = raiz;
                    //Si el nodo a extraer no está al principio de la lista avanzamos con una estructura repetitiva hasta el nodo anterior a extraer
                    for (int f = 1; f <= pos - 2; f++)
                        reco = reco.sig;
                    //definimos otro puntero auxiliar y lo disponemos en el siguiente nodo a donde está apuntando reco
                    Nodo prox = reco.sig;
                    //enlazamos el puntero sig del nodo apuntado por reco al nodo siguiente del nodo apuntado por prox con lo que que prox es eliminado
                    reco.sig = prox.sig;
                    informacion = prox.info;
                }
                //Devuelve la información pedida
                return informacion;
            }
            else
                return int.MaxValue;
        }
        //Creamos el método borrar que elimina un nodo y no devuelve su información, es la única diferencia con Extraer.
        public void Borrar(int pos)
        {

            if (pos <= Cantidad())
            {
                if (pos == 1)
                {
                    raiz = raiz.sig;
                }
                else
                {
                    Nodo reco;
                    reco = raiz;
                    for (int f = 1; f <= pos - 2; f++)
                        reco = reco.sig;
                    Nodo prox = reco.sig;
                    reco.sig = prox.sig;
                }
            }
        }

        //Intercambiar recibe dos enteros que representan las posiciones de los nodos que queremos intercambiar sus informaciones
        public void Intercambiar(int pos1, int pos2)
        {
            //Verificamos que las dos posiciones existan en la lista.
            if (pos1 <= Cantidad() && pos2 <= Cantidad())
            {
                //Definimos un puntero auxiliar llamado reco1, lo inicializamos con la dirección del primer nodo y mediante un for avanzamos hasta la posición almacenada en pos1
                Nodo reco1 = raiz;
                for (int f = 1; f < pos1; f++)
                    reco1 = reco1.sig;
                //De forma similar con un segundo puntero auxiliar avanzamos hasta la posición indicada por pos2
                Nodo reco2 = raiz;
                for (int f = 1; f < pos2; f++)
                    reco2 = reco2.sig;
                //intercambiamos las informaciones que almacenan cada nodo
                int aux = reco1.info;
                reco1.info = reco2.info;
                reco2.info = aux;
            }
        }

        //método que retorna el mayor de la lista
        public int Mayor()
        {
            //Verificamos que la lista no esté vacía
            if (!Vacia())
            {
                //Suponemos que el mayor es el primero de la lista e inicializamos un puntero auxiliar con la dirección del segundo nodo de la lista
                int may = raiz.info;
                Nodo reco = raiz.sig;
                //Mediante una estructura repetitiva recorremos toda la lista
                while (reco != null)
                {
                    //Cada vez que encontramos un nodo con información mayor que la variable may la actualizamos con este nuevo valor y avanzamos el puntero reco para visitar el siguiente nodo
                    if (reco.info > may)
                        may = reco.info;
                    reco = reco.sig;
                }
                //retornamos el mayor
                return may;
            }
            else
                return int.MaxValue;
        }

        //El método que retorna la posición del mayor es similar al anterior con la salvedad que debemos almacenar en otro auxiliar la posición donde se almacena el mayor
        public int PosMayor()
        {
            if (!Vacia())
            {
                int may = raiz.info;
                int x = 1;
                int pos = x;
                Nodo reco = raiz.sig;
                while (reco != null)
                {
                    if (reco.info > may)
                    {
                        may = reco.info;
                        pos = x;
                    }
                    reco = reco.sig;
                    x++;
                }
                //devolvemos posición
                return pos;
            }
            else
                return int.MaxValue;
        }

        //contamos cuantos nodos tiene la lista
        public int Cantidad()
        {
            //Variable que cuenta
            int cant = 0;
            //Asignamos el inicio para contar
            Nodo reco = raiz;
            //recorremos la lista para contar
            while (reco != null)
            {
                cant++;
                reco = reco.sig;
            }
            //devolvemos la cantidad
            return cant;
        }

        //si está ordenada la lista de menor a mayor
        public bool Ordenada()
        {
            //verificamos si la lista tiene más de un nodo
            if (Cantidad() > 1)
            {
                //Disponemos dos punteros auxiliares con las direcciones del primer y segundo nodo de la lista
                Nodo reco1 = raiz;
                Nodo reco2 = raiz.sig;

                while (reco2 != null)
                {
                    //controlamos si la información del segundo nodo es menor al nodo anterior significa que la lista no está ordenada y podemos parar el análisis retornando un false
                    if (reco2.info < reco1.info)
                    {
                        return false;
                    }
                    //avanzamos los dos punteros a sus nodos siguientes respectivamente
                    reco2 = reco2.sig;
                    reco1 = reco1.sig;
                }
            }
            //retornamos true indicando que la lista está ordenada de menor a mayor
            return true;
        }

        //Si existe un nodo
        public bool Existe(int x)
        {
            Nodo reco = raiz;
            //recorremos la lista
            while (reco != null)
            {
                //Preguntamos si existe
                if (reco.info == x)
                    return true;
                //REcogemos su dirección
                reco = reco.sig;
            }
            return false;
        }

        //Comprobamos si está vacía la lista
        public bool Vacia()
        {
            if (raiz == null)
                return true;
            else
                return false;
        }

        public void Imprimir()
        {
            Nodo reco = raiz;
            while (reco != null)
            {
                Console.Write(reco.info + "-");
                reco = reco.sig;
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            ListaGenerica lg = new ListaGenerica();
            lg.Insertar(1, 10);
            lg.Insertar(2, 20);
            lg.Insertar(3, 30);
            lg.Insertar(2, 15);
            lg.Insertar(1, 115);
            lg.Imprimir();
            Console.WriteLine("Luego de Borrar el primero");
            lg.Borrar(1);
            lg.Imprimir();
            Console.WriteLine("Luego de Extraer el segundo");
            lg.Extraer(2);
            lg.Imprimir();
            Console.WriteLine("Luego de Intercambiar el primero con el tercero");
            lg.Intercambiar(1, 3);
            lg.Imprimir();
            if (lg.Existe(10))
                Console.WriteLine("Se encuentra el 20 en la lista");
            else
                Console.WriteLine("No se encuentra el 20 en la lista");
            Console.WriteLine("La posición del mayor es:" + lg.PosMayor());
            if (lg.Ordenada())
                Console.WriteLine("La lista está ordenada de menor a mayor");
            else
                Console.WriteLine("La lista no está ordenada de menor a mayor");
            Console.ReadKey();
        }
    }


    /*
     Plantear una clase para administrar una lista genérica implementando los siguientes métodos:
a) Insertar un nodo al principio de la lista.
b) Insertar un nodo al final de la lista.
c) Insertar un nodo en la segunda posición. Si la lista está vacía no se inserta el nodo.
d) Insertar un nodo en la ante última posición.
e) Borrar el primer nodo.
f) Borrar el segundo nodo.
g) Borrar el último nodo.
h) Borrar el nodo con información mayor. 
     
     
     

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ListaGenerica2
{
    class ListaGenerica
    {
        class Nodo
        {
            public int info;
            public Nodo sig;
        }

        private Nodo raiz;

        public ListaGenerica()
        {
            raiz = null;
        }

        void InsertarPrimero(int x)
        {
            Nodo nuevo = new Nodo();
            nuevo.info = x;
            nuevo.sig = raiz;
            raiz = nuevo;
        }

        public void InsertarUtlimo(int x)
        {
            Nodo nuevo = new Nodo();
            nuevo.info = x;
            if (raiz == null)
                raiz = nuevo;
            else
            {
                Nodo reco = raiz;
                while (reco.sig != null)
                {
                    reco = reco.sig;
                }
                reco.sig = nuevo;
            }
        }

        public void InsertarSegundo(int x)
        {
            if (raiz != null)
            {
                Nodo nuevo = new Nodo();
                nuevo.info = x;
                if (raiz.sig == null)
                {
                    //Hay un solo nodo.
                    raiz.sig = nuevo;
                }
                else
                {
                    nuevo.sig = raiz.sig;
                    raiz.sig = nuevo;
                }
            }
        }

        public void InsertarAnteUltimo(int x)
        {
            if (raiz != null)
            {
                Nodo nuevo = new Nodo();
                nuevo.info = x;
                if (raiz.sig == null)
                {
                    //Hay un solo nodo.
                    nuevo.sig = raiz;
                    raiz = nuevo;
                }
                else
                {
                    Nodo atras = raiz;
                    Nodo reco = raiz.sig;
                    while (reco.sig != null)
                    {
                        atras = reco;
                        reco = reco.sig;
                    }
                    nuevo.sig = atras.sig;
                    atras.sig = nuevo;
                }
            }
        }

        public void BorrarPrimero()
        {
            if (raiz != null)
            {
                raiz = raiz.sig;
            }
        }

        public void BorrarSegundo()
        {
            if (raiz != null)
            {
                if (raiz.sig != null)
                {
                    Nodo tercero = raiz.sig;
                    tercero = tercero.sig;
                    raiz.sig = tercero;
                }
            }
        }

        public void BorrarUltimo()
        {
            if (raiz != null)
            {
                if (raiz.sig == null)
                {
                    raiz = null;
                }
                else
                {
                    Nodo reco = raiz.sig;
                    Nodo atras = reco;
                    while (reco.sig != null)
                    {
                        atras = reco;
                        reco = reco.sig;
                    }
                    atras.sig = null;
                }
            }

        }
        public void Imprimir() 
        {
            Nodo reco = raiz;
            while (reco != null)
            {
                Console.Write (reco.info + "-");
                reco = reco.sig;
            }
            Console.WriteLine();
        }

        public void BorrarMayor()
        {
            if (raiz != null)
            {
                Nodo reco = raiz;
                int may = raiz.info;
                while (reco != null)
                {
                    if (reco.info > may)
                    {
                        may = reco.info;
                    }
                    reco = reco.sig;
                }
                reco = raiz;
                Nodo atras = raiz;
                while (reco != null)
                {
                    if (reco.info == may)
                    {
                        if (reco == raiz)
                        {
                            raiz = raiz.sig;
                            atras = raiz;
                            reco = raiz;
                        }
                        else
                        {
                            atras.sig = reco.sig;
                            reco = reco.sig;
                        }
                    }
                    else
                    {
                        atras = reco;
                        reco = reco.sig;
                    }
                }
            }
        }


        static void Main(string[] args)
        {
            ListaGenerica lg=new ListaGenerica();
            lg.InsertarPrimero (10);
            lg.InsertarPrimero(45);
            lg.InsertarPrimero(23);
            lg.InsertarPrimero(89);
            lg.Imprimir();
            Console.WriteLine("Insertamos un nodo al final:");
            lg.InsertarUtlimo(160);
            lg.Imprimir();
            Console.WriteLine("Insertamos un nodo en la segunda posición:");
            lg.InsertarSegundo(13);
            lg.Imprimir();
            Console.WriteLine("Insertamos un nodo en la anteultima posición:");
            lg.InsertarAnteUltimo(600);
            lg.Imprimir();
            Console.WriteLine("Borramos el primer nodo de la lista:");
            lg.BorrarPrimero();
            lg.Imprimir();        
            Console.WriteLine("Borramos el segundo nodo de la lista:");
            lg.BorrarSegundo();
            lg.Imprimir();
            Console.WriteLine("Borramos el ultimo nodo de la lista:");
            lg.BorrarUltimo();
            lg.Imprimir();                
            Console.WriteLine("Borramos el mayor de la lista:");
            lg.BorrarMayor();
            lg.Imprimir();
            Console.ReadKey();        
        }
    }
}

     */

}
