﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _31ListaPila
{
    //Confeccionar una clase que administre una lista tipo pila (se debe poder insertar, extraer e imprimir los datos de la pila)
    class Pila
    {
        //Creamos el Nodo.Para declarar un nodo debemos utilizar una clase
        class Nodo
        {
            //Primera parte es la información que contiene
            public int info;
            //Segunda parte, tiene la información del siguiente nodo o null si no hay otro nodo
            public Nodo sig;
        }

        //Variable raiz para marcar el primer nodo de la lista
        private Nodo raiz;

        //Le damos el valor de null para establecer el último nodo.
        public Pila()
        {
            raiz = null;
        }
        //Creamos la clase que introduce los nodos en la lista
        public void Insertar(int x)
        {
            //Definición de un puntero o referencia a un tipo de dato Nodo. 
            Nodo nuevo;
            //Creamos el objeto.
            nuevo = new Nodo();
            //Se le asigna la información al nodo.
            nuevo.info = x;
            //Si es el primer nodo
            if (raiz == null)
            {
                //se guarda null para indicar que es el primero
                nuevo.sig = null;
                //Lo establecemos como primero
                raiz = nuevo;
            }
            //si ya existe algun nodo en la lista
            else
            {
                //Al nuevo que entra se le asigna que es el raiz
                nuevo.sig = raiz;
                //A la raiz le indicamos que este nuevo es el primero de la lista para salir
                raiz = nuevo;
            }
        }

        //Creamos el método para extraer el último que ha entrado en la lista y borrarlo
        public int Extraer()
        {
            //Si no está vacía
            if (raiz != null)
            {
                //Guardamos en una variable local la información del primer nodo.
                int informacion = raiz.info;
                //Avanzamos raiz al segundo nodo de la lista. El primer nodo desaparece al perder el raiz se elimina automáticamente.
                raiz = raiz.sig;
                //Devolvemos la información del nodo eliminado. 
                return informacion;
            }
            //Si está vacía
            else
            {
                //retornamos el número entero máximo y lo tomamos como código de error.
                return int.MaxValue;
            }
        }
        //Vamos a mostrar el interior de la lista
        public void Imprimir()
        {
            //Creamos objeto para manejar la raiz
            Nodo reco = raiz;
            //Mostramos
            Console.WriteLine("Listado de todos los elementos de la pila.");
            //Mientras haya un nodo entra
            while (reco != null)
            {
                //Escribe la información
                Console.Write(reco.info + "-");
                //Se asigna la dirección del raiz al nuevo nodo
                reco = reco.sig;
            }
            Console.WriteLine();
        }

        public bool Vacia()
        {
            //Si está vacía
            if (raiz == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //contamos cuantos nodos tiene la lista
        public int Cantidad()
        {
            //Variable que cuenta
            int cant = 0;
            //Asignamos el inicio para contar
            Nodo reco = raiz;
            //recorremos la lista para contar
            while (reco != null)
            {
                cant++;
                reco = reco.sig;
            }
            //devolvemos la cantidad
            return cant;
        }
        static void Main(string[] args)
        {
            //Creamos la lista
            Pila pila1 = new Pila();
            //Insertar los elementos de la lista
            pila1.Insertar(10);
            pila1.Insertar(40);
            pila1.Insertar(3);
            //Mostramos los elementos de la lista
            pila1.Imprimir();
            //Quitamos un elemento de la pila
            Console.WriteLine("Extraemos de la pila:" + pila1.Extraer());
            //Imprimimos los nodos que quedan
            pila1.Imprimir();
            Console.ReadKey();
        }
    }
}
