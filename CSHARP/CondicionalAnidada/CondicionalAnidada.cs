﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Confeccionar un programa que pida por teclado tres notas de un alumno, calcule el promedio e imprima alguno de estos mensajes:
Si el promedio es >=7 mostrar "Promocionado".
Si el promedio es >=4 y <7 mostrar "Regular".
Si el promedio es <4 mostrar "Reprobado". */

namespace EstructuraCondicionalAnidada1
{
    class CondicionalAnidada
    {
        static void Main(string[] args)
        {
            int nota1, nota2, nota3;
            string linea;
            Console.Write("Ingrese primer nota:");
            linea = Console.ReadLine();
            nota1 = int.Parse(linea);
            Console.Write("Ingrese segunda nota:");
            linea = Console.ReadLine();
            nota2 = int.Parse(linea);
            Console.Write("Ingrese tercer nota:");
            linea = Console.ReadLine();
            nota3 = int.Parse(linea);
            //Operacion para calcular la media.
            int promedio = (nota1 + nota2 + nota3) / 3;
            //Primera condición, si es mayor o igual a 7
            if (promedio >= 7)
            {
                Console.Write("Promocionado");
            }
            //No se cumple lo anterior entramos en la nueva condición.
            else
            {
                //La condición de los números que no entran en la primera condición.
                if (promedio >= 4)
                {
                    //Si se cumple que es mayor o igual que 4, pero también cumple la condición anterior
                    Console.Write("Regular");
                }
                //Si no cumple ninguna de las anteriores entonces entra aquí.
                else
                {
                    Console.Write("Reprobado");
                }
            }
            Console.ReadKey();
        }
    }
}
/*Ejercicios:
 * 1.- Se cargan por teclado tres números distintos. Mostrar por pantalla el mayor de ellos.
 * 2.- Se ingresa por teclado un valor entero, mostrar una leyenda que indique si el número es positivo, nulo o negativo.
 * 3.- Confeccionar un programa que permita cargar un número entero positivo de hasta tres cifras y muestre un mensaje indicando si tiene 1, 2, o 3 cifras. Mostrar un mensaje de error si el número de cifras es mayor.
 * 4.- Un postulante a un empleo, realiza un test de capacitación, se obtuvo la siguiente información: cantidad total de preguntas que se le realizaron y la cantidad de preguntas que contestó correctamente. Se pide confeccionar un programa que ingrese los dos datos por teclado e informe el nivel del mismo según el porcentaje de respuestas correctas que ha obtenido, y sabiendo que:

	Nivel máximo:	Porcentaje>=90%.
	Nivel medio:	Porcentaje>=75% y <90%.
	Nivel regular:	Porcentaje>=50% y <75%.
	Fuera de nivel:	Porcentaje<50%.





















using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalAnidada2
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1,num2,num3;
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1=int.Parse(linea);
            Console.Write("Ingrese segunda valor:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese tercer valor:");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            if (num1>num2) 
            {
                if (num1>num3) 
                {
                    Console.Write(num1);
                } 
                else 
                {
                    Console.Write(num3);
                }
            } 
            else 
            {
                if (num2>num3) 
                {
                    Console.Write(num2);
                }
                else 
                {
                    Console.Write(num3);
                }
            }
            Console.ReadKey();
        }
    }
}




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalAnidada3
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            string linea;
            Console.Write("Ingrese un valor:");
            linea = Console.ReadLine();
            num=int.Parse(linea);
            if (num==0) 
            {
	            Console.Write("Se ingresó el cero");
	        } 
            else 
            {
	            if (num>0) 
                {
	                Console.Write("Se ingresó un valor positivo");
	            } 
                else 
                {
	                Console.Write("Se ingresó un valor negativo");
	            }
	        }
            Console.ReadKey();
        }
    }
}





using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalAnidada4
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            string linea;
            Console.Write("Ingrese un valor de hasta tres dígitos positivo:");
            linea = Console.ReadLine();
            num=int.Parse(linea);
            if (num<10) 
            {
	            Console.Write("Tiene un dígito");
            }
            else 
            {
                if (num<100) 
                {
                    Console.Write("Tiene dos dígitos");
                }
                else 
                {
                    if (num<1000) 
                    {
                        Console.Write("Tiene tres dígitos");
                    }
                    else 
                    {
                        Console.Write("Error en la entrada de datos.");
                    }
                }
            }
            Console.ReadKey();
        }
    }
}




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalAnidada5
{
    class Program
    {
        static void Main(string[] args)
        {
            int totalPreguntas,totalCorrectas;
            string linea;
            Console.Write("Ingrese la cantidad total de preguntas del examen:");
            linea = Console.ReadLine();
            totalPreguntas=int.Parse(linea);
            Console.Write("Ingrese la cantidad total de preguntas contestadas correctamente:");
            linea = Console.ReadLine();
            totalCorrectas=int.Parse(linea);
            int porcentaje=totalCorrectas * 100 / totalPreguntas;
            if (porcentaje>=90) 
            {
                Console.Write("Nivel máximo");
            } 
            else 
            {
                if (porcentaje>=75) 
                {
                    Console.Write("Nivel medio");
                }
                else
                {
                    if (porcentaje>=50) 
                    {
                        Console.Write("Nivel regular");
                    }
                    else 
                    {
                        Console.Write("Fuera de nivel");
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
 * */