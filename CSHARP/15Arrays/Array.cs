﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector1
{
    //Se desea guardar los sueldos de 5 operarios.
    class PruebaVector1
    {
        //Declaramos el vector o Array indicando el tipo de variable que se va a introducir y le ponemos un nombre.
        //Se hace private para utilizarlo solo en esta clase y sus clases inferiores.
        private int[] sueldos;

        public void Cargar()
        {
            //Inicializamos el Array diciéndole la cantidad de elemento a introducir.
            sueldos = new int[5];
            //Utilizamos un for para hacer el ciclo de pedir los datos para introducirlos en el array
            for (int f = 0; f < 5; f++)
            {
                //Ponemos f+1 porque empezamos en 0 y lo lógico es pedir el primer sueldo y a partir de ahí.
                Console.Write("Ingrese valor del sueldo " + (f+1) + " :");
                String linea;
                linea = Console.ReadLine();
                //Aquí se le asigna con f el número de índice a introducir por eso lo hemos empezado en cero, ya que es el primer índice del array.
                Console.WriteLine("Índice: " + f);
                sueldos[f] = int.Parse(linea);
            }
        }

        public void Imprimir()
        {
            //Utilizamos también el for para leerlo y mostrar las variables
            for (int f = 0; f < 5; f++)
            {
                Console.WriteLine("Índice: " + f);
                //Muestra cada valor de cada variable de cada índice, como empieza en cero hay que sumarle uno.
                Console.WriteLine("El sueldo " + (f+1) + " es: " +sueldos[f]);
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            PruebaVector1 pv = new PruebaVector1();
            pv.Cargar();
            pv.Imprimir();
        }
    }
}

/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector2
{
    //Definir un vector de 5 componentes de tipo float que representen las alturas de 5 personas. Obtener el promedio de las mismas. Contar cuántas personas son más altas que el promedio y cuántas más bajas.
    class PruebaVector2
    {
        //Declaramos el array de tipo float, private y externo al las clases inferiores.
        private float[] alturas;
        //Declaramos una variable tipo float, private y externo al las clases inferiores.
        private float promedio;

        //Clase para cargar las alturas.
        public void Cargar()
        {
            //Inicializamos el Array diciéndole la cantidad de elemento a introducir.
            alturas = new float[5];
            //Empezamos el for para rellenar el array.
            for (int f = 0; f < 5; f++)
            {
                Console.Write("Ingrese la altura de la persona " + (f+1) +" : ");
                string linea = Console.ReadLine();
                //Introducimos el dato convertido en el array en el índice que corresponde.
                alturas[f] = float.Parse(linea);
            }
        }

        //Clase para calcular el promedio.
        public void CalcularPromedio()
        {
            //Declaramos la variable para sumar todas las alturas.
            float suma;
            //La inicializamos para que al sumarle algo, no encuentre null.
            suma = 0;
            //Declaramos f fuera del for para poder utilizarlo después al mostrar el dato en pantalla.
            int f;
            //USamos el for para sumar las alturas de dentro del array.
            for (f = 0; f < 5; f++)
            {
                suma += alturas[f];
            }
            //Se calcula la media dividiendo la suma por el número de elementos.
            promedio = suma / 5;
            //Mostramos el promedio.
            Console.WriteLine("El promedio de las 5 alturas:" + promedio);
        }

        //Clase para saber cuantos son mayores a la media y cuentos son menores.
        public void MayoresMenores()
        {
            //Declaramos las variables contadores de más altos y menos.
            int may, men;
            //Los inicializamos para que no tenga un null dentro.
            may = 0;
            men = 0;
            //Iniciamos el for para recorrer el array y comparar las alturas.
            for (int f = 0; f < 5; f++)
            {
                //Si es más alto entramos 
                if (alturas[f] > promedio)
                {
                    may++;
                }
                //Si no, no entramos
                else
                {
                    if (alturas[f] < promedio)
                    {
                        men++;
                    }
                }
            }
            //Mostramos resultados.
            Console.WriteLine("Cantidad de personas mayores al promedio:" + may);
            Console.WriteLine("Cantidad de personas menores al promedio:" + men);
            Console.ReadKey();
        }

        //Clase de Inicio de todas las subclases.
        static void Main(string[] args)
        {
            PruebaVector2 pv2 = new PruebaVector2();
            pv2.Cargar();
            pv2.CalcularPromedio();
            pv2.MayoresMenores();
        }
    }
}





Ejemplo 1

Una empresa tiene dos turnos (mañana y tarde) en los que trabajan 8 empleados (4 por la mañana y 4 por la tarde)
Confeccionar un programa que permita almacenar los sueldos de los empleados agrupados por turno.
Imprimir los gastos en sueldos de cada turno.




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector3
{
    class PruebaVector3
    {
        private float[] turnoMan;
        private float[] turnoTar;

        public void Cargar() 
        {
            string linea;
            turnoMan=new float[4];
            turnoTar=new float[4];
            Console.WriteLine("Sueldos de empleados del turno de la mañana.");
            for(int f = 0; f < 4; f++) 
            {
                Console.Write("Ingrese sueldo:");
                linea = Console.ReadLine();              
                turnoMan[f]=float.Parse(linea);
            }
            Console.WriteLine("Sueldos de empleados del turno de la tarde.");
            for(int f = 0; f < 4; f++) 
            {
                Console.Write("Ingrese sueldo:");
                linea = Console.ReadLine();              
                turnoTar[f]=float.Parse(linea);
            }
        }

        public void CalcularGastos() 
        {
            float man=0;
            float tar=0;
            for(int f = 0; f < 4; f++)
            {
                man=man+turnoMan[f];
                tar=tar+turnoTar[f];
            }
            Console.WriteLine("Total de gastos del turno de la mañana:"+man);
            Console.WriteLine("Total de gastos del turno de la tarde:"+tar);
            Console.ReadKey();
        }


        static void Main(string[] args)
        {
            PruebaVector3 pv = new PruebaVector3();
            pv.Cargar();
            pv.CalcularGastos();
        }
    }
}

Ejemplo 2

 Desarrollar un programa que permita ingresar un array de 8 elementos, e informe:
El valor acumulado de todos los elementos del array.
El valor acumulado de los elementos del array que sean mayores a 36.
Cantidad de valores mayores a 50.

 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector4
{
    class PruebaVector4
    {
        private int[] vec;

        public void Cargar() 
        {
           vec=new int[8];
           for(int f = 0; f < 8; f++) 
           {
                Console.Write("Ingrese elemento:");
                string linea;
                linea = Console.ReadLine();
                vec[f]=int.Parse(linea);
            }
        }

        public void AcumularElementos() 
        {
            int suma=0;
            for(int f = 0; f < 8; f++) 
            {
                suma=suma+vec[f];
            }
            Console.WriteLine("La suma de los 8 elementos es:"+suma);
        }

        public void AcumularMayores36() 
        {
            int suma=0;
            for(int f = 0; f < 8; f++) 
            {
                if (vec[f] > 36) 
                {
                    suma=suma+vec[f];
                }
            }
            Console.WriteLine("La suma de los elementos mayores a 36 es:"+suma);
        }

        public void CantidadMayores50() 
        {
            int cant=0;
            for(int f = 0; f < 8; f++) 
            {
                if (vec[f] > 50) 
                {
                    cant++;
                }    
            }
            Console.WriteLine("La cantidad de valores mayores a 50 es:"+cant);
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            PruebaVector4 pv = new PruebaVector4();
            pv.Cargar();
            pv.AcumularElementos();
            pv.AcumularMayores36();
            pv.CantidadMayores50();
        }
    }
}

Realizar un programa que pida la carga de dos arrays numéricos enteros de 4 elementos. Obtener la suma de los dos arrays, dicho resultado guardarlo en un tercer arrays del mismo tamaño. Sumar componente a componente. 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector5
{
    class PruebaVector5
    {
        private int[] vec1;
        private int[] vec2;
        private int[] vecSuma;

        public void Cargar() 
        {
            vec1=new int[4];
            vec2=new int[4];
            Console.WriteLine("Carga del primer array.");
            for(int f = 0;f < 4; f++) 
            {
                Console.Write("Ingrese elemento:");
                string linea;
                linea=Console.ReadLine();
                vec1[f]=int.Parse(linea);
            }
            Console.WriteLine("Carga del segundo array.");
            for(int f = 0; f < 4; f++) 
            {
                Console.Write("Ingrese elemento:");
                string linea;
                linea = Console.ReadLine();
                vec2[f] = int.Parse(linea);
            }        
        }
            
        public void SumarizarVectores() 
        {
    	    vecSuma=new int[4];
            for(int f = 0;f < 4; f++)
            {
                vecSuma[f]=vec1[f]+vec2[f];
            }
            Console.WriteLine("Vector resultante.");
            for(int f = 0; f < 4; f++)
            {
                Console.WriteLine(vecSuma[f]);
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            PruebaVector5 pv = new PruebaVector5();
            pv.Cargar();
            pv.SumarizarVectores();
        }
    }
}

Se tienen las notas del primer parcial de los alumnos de dos cursos, el curso A y el curso B, cada curso cuenta con 5 alumnos.
Realizar un programa que muestre el curso que obtuvo el mayor promedio general. 



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector6
{
    class PruebaVector6
    {
        private int[] cursoa;
        private int[] cursob;

        public void Cargar() 
        {
            cursoa=new int[5];
            cursob=new int[5];
            Console.WriteLine("Carga de notas del curso A");
            for(int f = 0; f < 5; f++) 
            {
                Console.Write("Ingrese nota:");
                string linea;
                linea = Console.ReadLine();
                cursoa[f]=int.Parse(linea);
            }
            Console.WriteLine("Carga del notas del curso B");
            for(int f = 0; f < 5; f++) 
            {
                Console.Write("Ingrese nota:");
                string linea;
                linea = Console.ReadLine();
                cursob[f] =int.Parse(linea);
            }        
        }

        public void CalcularPromedios() 
        {
            int suma1=0;
            int suma2=0;
            for(int f=0;f<5;f++) 
            {
                suma1=suma1+cursoa[f];
                suma2=suma2+cursob[f];            
            }
            int promedioa=suma1/5;
            int promediob=suma2/5;
            if (promedioa>promediob) 
            {
                Console.WriteLine("El curso A tiene un promedio mayor.");
            }
            else
            {
                Console.WriteLine("El curso B tiene un promedio mayor.");
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            PruebaVector6 pv = new PruebaVector6();
            pv.Cargar();
            pv.CalcularPromedios();
        }
    }
}



Cargar un array de 10 elementos y verificar posteriormente si el mismo está ordenado de menor a mayor.



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector7
{
    class PruebaVector7
    {
        private int[] vec;

        public void Cargar()
        {
            vec=new int[10];
            for(int f = 0; f < 10; f++)
            {
                Console.Write("Ingrese elemento:");
                string linea = Console.ReadLine();
                vec[f]=int.Parse(linea);
            }
        }

        public void VerificarOrdenado() 
        {
            int orden=1;
            for(int f = 0; f < 9; f++) 
            {
                if (vec[f+1] < vec[f]) 
                {
                    orden=0;
                }
            }
            if (orden==1) 
            {
                Console.WriteLine("Esta ordenado de menor a mayor");
            }
            else 
            {
                Console.WriteLine("No esta ordenado de menor a mayor");
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            PruebaVector7 pv = new PruebaVector7();
            pv.Cargar();
            pv.VerificarOrdenado();
        }
    }
}
 */