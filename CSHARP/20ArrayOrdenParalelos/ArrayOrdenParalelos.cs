﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20ArrayOrdenParalelos
{
    //Confeccionar un programa que permita cargar los nombres de 5 alumnos y sus notas respectivas. Luego ordenar las notas de mayor a menor. Imprimir las notas y los nombres de los alumnos.
    class ArrayOrdenParalelos
    {
        //Definimos dos arrays
        private string[] nombres;
        private int[] notas;

        //Creación del método Cargar que recoge los datos del teclado
        public void Cargar()
        {
            //Creamos los arrays dándoles un longitud
            nombres = new string[5];
            notas = new int[5];
            Console.WriteLine("Carga de nombres y notas");
            for (int f = 0; f < nombres.Length; f++)
            {
                Console.Write("Ingrese el nombre del alumno:");
                nombres[f] = Console.ReadLine();
                Console.Write("Ingrese la nota del alumno:");
                string linea;
                linea = Console.ReadLine();
                notas[f] = int.Parse(linea);
            }
        }

        //Método de ordenar
        public void Ordenar()
        {
            //En el proceso de ordenamiento dentro de los dos for verificamos si debemos intercambiar los elementos del vector notas:
            for (int k = 0; k < notas.Length; k++)
            {

                for (int f = 0; f < notas.Length - 1 - k; f++)
                {
                    if (notas[f] < notas[f + 1])
                    {
            //En el caso que la nota de la posición 'f' sea menor a de la posición siguiente 'f+1' procedemos a intercambiar las notas:
                        int auxnota;
                        auxnota = notas[f];
                        notas[f] = notas[f + 1];
                        notas[f + 1] = auxnota;
             //y simultánemamente procedemos a intercambiar los elementos del vector paralelo (con esto logramos que los dos vectores continuen siendo vectores paralelos):
                        string auxnombre;
                        auxnombre = nombres[f];
                        nombres[f] = nombres[f + 1];
                        nombres[f + 1] = auxnombre;
                    }
                }
            }
        }

        //Creamos método imprimir
        public void Imprimir()
        {
            Console.WriteLine("Nombres de alumnos y notas de mayor a menor");
            for (int f = 0; f < notas.Length; f++)
            {
                Console.WriteLine(nombres[f] + " - " + notas[f]);
            }
            Console.ReadLine();
        }

        //Inicio del programa
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase para poder llamar y ejecutar los métodos
            ArrayOrdenParalelos pv = new ArrayOrdenParalelos();
            //Llamamos a Cargar
            pv.Cargar();
            //Llamamos a Ordenar
            pv.Ordenar();
            //Llamamos a Imprimir
            pv.Imprimir();
        }
    }
    */
    /*
     Cargar en un array los nombres de 5 paises y en otro vector paralelo la cantidad de habitantes del mismo. Ordenar alfabéticamente e imprimir los resultados. Por último ordenar con respecto a la cantidad de habitantes (de mayor a menor) e imprimir nuevamente. 
*/
    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20ArrayOrdenParalelos
{
    class ArrayOrdenParalelos
    {
        private string[] paises;
        private int[] habitantes;

        public void Cargar() 
        {
            paises=new string[5];
            habitantes=new int[5];
            Console.WriteLine("Carga de paises y habitantes");
            for(int f=0;f < paises.Length;f++) 
            {
                Console.Write("Ingrese el nombre del pais:");
                paises[f]=Console.ReadLine();
                Console.Write("Ingrese la cantidad de habitantes:");
                string linea;
                linea = Console.ReadLine();
                habitantes[f]=int.Parse(linea);
            }
        }

        public void OrdenarPorNombres()
        {
            for (int k = 0; k < paises.Length; k++)
            {
                for (int f = 0; f < paises.Length - 1 - k; f++)
                {
                    if (paises[f].CompareTo(paises[f + 1]) > 0)
                    {
                        string auxpais;
                        auxpais = paises[f];
                        paises[f] = paises[f + 1];
                        paises[f + 1] = auxpais;
                        int auxhabitante;
                        auxhabitante = habitantes[f];
                        habitantes[f] = habitantes[f + 1];
                        habitantes[f + 1] = auxhabitante;
                    }
                }
            }
        }

        public void OrdenarPorHabitantes()
        {
            for (int k = 0; k < paises.Length; k++)
            {
                for (int f = 0; f < paises.Length - 1 - k; f++)
                {
                    if (habitantes[f] < habitantes[f + 1])
                    {
                        string auxpais;
                        auxpais = paises[f];
                        paises[f] = paises[f + 1];
                        paises[f + 1] = auxpais;
                        int auxhabitante;
                        auxhabitante = habitantes[f];
                        habitantes[f] = habitantes[f + 1];
                        habitantes[f + 1] = auxhabitante;
                    }
                }
            }
        }

        public void Imprimir() 
        {
            for(int f=0;f < paises.Length;f++) 
            {
                Console.WriteLine(paises[f] + " - " + habitantes[f]);
            }
        }

        static void Main(string[] args)
        {
            ArrayOrdenParalelos pv = new ArrayOrdenParalelos();
            pv.Cargar();
            pv.OrdenarPorNombres();
            Console.WriteLine("Ordenados alfabéticamente");
            pv.Imprimir();
            pv.OrdenarPorHabitantes();
            Console.WriteLine("Ordenados por cantidad de habitnates");
            pv.Imprimir();
            Console.ReadKey();
        }
    }
}
     /*
}*/
