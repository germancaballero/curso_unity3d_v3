﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _29ConstructoresHerencia
//Plantear tres clases A, B, C que B herede de A y C herede de B. Definir un constructor a cada clase que muestre un mensaje. Luego definir un objeto de la clase C.
{
    //Creación de Clase A, la principal de la que se hereda
    public class A
    {
        public A()
        {
            Console.WriteLine("Constructor de la clase A");
        }
    }

    //Creación de la subclase que hereda de A y por eso ejecuta primero A
    public class B : A
    {
        public B()
        {
            Console.WriteLine("Constructor de la clase B");
        }
    }

    //Creación de la subclase que hereda de B y por eso ejecuta primero B
    public class C : B
    {
        public C()
        {
            Console.WriteLine("Constructor de la clase C");
        }
    }
    //Inicio de programa
    class Prueba
    {
        static void Main(string[] args)
        {
            //Creamos el objeto y llamamos a C
            C obj1 = new C();
            Console.ReadKey();
        }
    }

    /*
     Plantear tres clases A, B, C que B herede de A y C herede de B. Definir un constructor a cada clase que reciba como parámetro un entero. Luego definir un objeto de la clase C.

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia4
{
    public class A
    {
        public A(int a)
        {
            Console.WriteLine(a);
        }
    }

    public class B : A
    {
        public B(int b):base(b/2)
        {
            Console.WriteLine(b);
        }
    }

    public class C : B
    {
        public C(int c):base(c/2)
        {
            Console.WriteLine(c);
        }
    }

    class Prueba
    {
        static void Main(string[] args)
        {
            C obj1 = new C(20);
            Console.ReadKey();
        }
    }
}
     */
}
