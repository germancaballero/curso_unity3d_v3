﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _25ConstructorClase
{
    //Se desea guardar los sueldos de 5 operarios en un vector. Realizar la creación y carga del vector en el constructor.
    class ConstructorClase
    {
        //Declaramos el array
        private int[] sueldos;
        //Creamos el constructor que es un método que se ejecuta inicialmente y en forma automática. Tiene el mismo nombre de la clase. No puede retornar datos. Se ejecuta una única vez.
        public ConstructorClase()
        {
            //Creamos el array y le damos una dimensión
            sueldos = new int[5];
            //Iniciamos el for y pedimos los datos al usuario
            for (int f = 0; f < sueldos.Length; f++)
            {
                Console.Write("Ingrese el sueldo:");
                string linea = Console.ReadLine();
                //Guardamos los datos en el array
                sueldos[f] = int.Parse(linea);
            }
        }

        //Creamos el método imprimir para mostrar los datos
        public void Imprimir()
        {
            //Iniciamos el for y mostramos los datos
            for (int f = 0; f < sueldos.Length; f++)
            {
                Console.WriteLine(sueldos[f]);
            }
            Console.ReadKey();
        }

        //Iniciamos el programa
        static void Main(string[] args)
        {
            //Creamos el objeto para llamar a Imprimir ya que no hay que llamar al constructor.
            ConstructorClase op = new ConstructorClase();
            op.Imprimir();
        }
    }

    /*
     Plantear una clase llamada Alumno y definir como atributos su nombre y su edad. En el constructor realizar la carga de datos. Definir otros dos métodos para imprimir los datos ingresados y un mensaje si es mayor o no de edad (edad >=18)

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaConstructor2
{
    class Alumno
    {
        private string nombre;
        private int edad;

        public Alumno() 
        {
            Console.Write("Ingrese nombre:");
            nombre = Console.ReadLine();
            Console.Write("Ingrese edad:");
            string linea = Console.ReadLine();
            edad=int.Parse(linea);
        }

        public void Imprimir()
        {
            Console.WriteLine("Nombre:"+nombre);
            Console.WriteLine("Edad:"+edad);
        }

        public void EsMayorEdad() 
        {
            if (edad >= 18) 
            {
                Console.Write(nombre+" es mayor de edad.");
            }
            else 
            {
                Console.Write(nombre+" no es mayor de edad.");
            }
        }

        static void Main(string[] args)
        {
            Alumno alumno1 = new Alumno();
            alumno1.Imprimir();
            alumno1.EsMayorEdad();
            Console.ReadKey();
        }
    }
}



    Confeccionar una clase que represente un empleado. Definir como atributos su nombre y su sueldo. En el constructor cargar los atributos y luego en otro método imprimir sus datos y por último uno que imprima un mensaje si debe pagar impuestos (si el sueldo supera a 3000) 



    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaConstructor3
{
    class EmpleadoFabrica
    {
        string nombre;
        float sueldo;
    
        public EmpleadoFabrica() 
        {
            Console.Write("Ingrese el nombre del empleado:");
            nombre = Console.ReadLine();
            Console.Write("Ingrese su sueldo:");
            string linea = Console.ReadLine();
            sueldo = float.Parse(linea);
        }
    
        public void PagaImpuestos() 
        {
            if (sueldo > 3000)
            {
                Console.Write("Debe abonar impuestos");
            }
            else 
            {
                Console.Write("No paga impuestos");
            }
            Console.ReadKey();
        }
    
        static void Main(string[] args)
        {
            EmpleadoFabrica empleado1;
            empleado1 = new EmpleadoFabrica();
            empleado1.PagaImpuestos();
        }
    }
}



    Implementar la clase operaciones. Se deben cargar dos valores enteros en el constructor, calcular su suma, resta, multiplicación y división, cada una en un método, imprimir dichos resultados. 



    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaConstructor4
{
    class OperacionesCalculo
    {
        int valor1, valor2;

        public OperacionesCalculo() 
        {
            Console.Write("Ingrese primer valor:");
            string linea = Console.ReadLine();
            valor1=int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            valor2=int.Parse(linea);
        }

        public void Sumar() 
        {
            int suma;
            suma=valor1+valor2;
            Console.WriteLine("La suma es:"+suma);
        }

        public void Restar() 
        {
            int resta;
            resta=valor1-valor2;
            Console.WriteLine("La resta es:"+resta);
        }

        public void Multiplicar() 
        {
            int multiplicacion;
            multiplicacion=valor1*valor2;
            Console.WriteLine("La multiplicación es:"+multiplicacion);
        }

        public void Dividir()
        {
            int division;
            division=valor1/valor2;
            Console.WriteLine("La división es:"+division);
        }

        static void Main(string[] args)
        {
            OperacionesCalculo opera = new OperacionesCalculo();
            opera.Sumar();
            opera.Restar();
            opera.Multiplicar();
            opera.Dividir();
            Console.ReadKey();
        }
    }
}
     */
}
