﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//Es utilizada para recorrer colecciones de datos (por ejemplo arrays)

namespace _35Foreach
{
    //Almacenar los sueldos de 5 operarios en un vector, imprimir los elementos recorriendo el vector con la estructura repetitiva foreach.
    class Foreach
    {
        private int[] sueldos;

        public void Cargar()
        {
            //Creamos e inicializamos un array de 5 elemento
            sueldos = new int[5];
            //Utilizamos el for para rellenar el array
            for (int f = 0; f < 5; f++)
            {
                Console.Write("Ingrese valor de la componente:");
                String linea;
                linea = Console.ReadLine();
                sueldos[f] = int.Parse(linea);
            }
        }

        public void Imprimir()
        {
            //La variable s almacena la primera vez el primer elemento del vector sueldos, seguidamente se ejecuta el bloque del foreach, funciona mientras s tenga algún dato
            foreach (int s in sueldos)
            {
                Console.WriteLine(s);
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Foreach pv = new Foreach();
            pv.Cargar();
            pv.Imprimir();
        }
    }

    /*
     * Crear un array de n elementos de tipo entero (n se ingresa por teclado) Mostrar cuantos elementos son superiores a 100 (emplear el foreach para recorrer el vector) 
     * 
     * 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Estructuraforeach2
{
    class Mayores100
    {
        private int[] vec;

        public void Cargar()
        {
            Console.Write("Cantidad de elementos:");
            int n = int.Parse(Console.ReadLine());
            vec = new int[n];
            for (int f = 0; f < vec.Length; f++)
            {
                Console.Write("Ingrese valor de la componente:");
                String linea;
                linea = Console.ReadLine();
                vec[f] = int.Parse(linea);
            }
        }

        public void Imprimir()
        {
            int cant = 0;
            foreach (var valor in vec)
            {
                if (valor > 100)
                {
                    cant++;
                }
            }
            Console.Write("La cantidad de elementos mayores a 100 son:" + cant);
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Mayores100 m = new Mayores100();
            m.Cargar();
            m.Imprimir();
        }
    }
}     * */

}
