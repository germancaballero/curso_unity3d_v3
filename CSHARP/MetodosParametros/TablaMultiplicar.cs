﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosParametros
{
    //Los metodos son clases, pero con parámetros.
    //Los parámetros los podemos imaginar como variables locales al método, pero su valor se inicializa con datos que llegan cuando lo llamamos.



    //Confeccionar una clase que permita ingresar valores enteros por teclado y nos muestre la tabla de multiplicar de dicho valor. Finalizar el programa al ingresar el -1.
    class TablaMultiplicar
    {
        
        //Creamos la primera función que nos va a cargar el valor que nos introduce el usuario.
        public void CargarValor()
        {
            //Declaramos variables
            int valor;
            string linea;
            //Iniciamos el Do while por si no le interesa hacer la tabla de multiplicar e introduce -1.
            do
            {
                //Le pedimos el dato al usuario.
                Console.Write("Ingrese un valor (-1 para finalizar):");
                //Lo leemos
                linea = Console.ReadLine();
                //Lo introducimos convertido.
                valor = int.Parse(linea);
                //Preguntamos si quiere hacer la tabla de multiplicar.
                if (valor != -1)
                    //Si es distinto que -1 entramos.
                {
                    //Llamamos a una función, pero le enviamos el valor que hemos obtenido, por eso se llama método.
                    Calcular(valor);
                }
                //Se repite la creación de tablas de multiplicar mientras introduzca números distintos de -1.
            } while (valor != -1);

        }

        //Método al que hemos llamado desde la función CagarValor con un dato. Hay que declarar una variable para usar dentro del método del mismo tipo de variable que hemos recibido.Que a la vez se le introduce el dato a la vez que se le declara.
        public void Calcular(int valor1)
        {
            //Iniciamos el método con el dato recibido, en este caso, iniciamos un for

            for (int f = 0; f <= 10; f ++)
            {
                Console.WriteLine("-" + valor1 + "x" + f + "=" + (valor1*f));
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            TablaMultiplicar tm = new TablaMultiplicar();
            tm.CargarValor();
            Console.ReadKey();

        }
    }
}
