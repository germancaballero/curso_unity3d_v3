﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*C# dispone de un mecanismo de capturar (catch) ciertos tipos de errores que solo pueden ser detectados en tiempo de ejecución del programa.
 */
namespace _39TryCatch
{
    //Realizar la carga de un número entero por teclado e imprimir su cuadrado.
   class Program
    {
        static void Main(string[] args)
        {
    //Iniciamos la parte del programa donde se captura el error
            try {
                Console.Write("Ingrese un valor:");
                string linea = Console.ReadLine();
                var num = int.Parse(linea);
                var cuadrado = num * num;
                Console.WriteLine($"El cuadrado de {num} es {cuadrado}");
            }
    //Ahora recoge todas las excepciones y le da una alternativa que no de error y no se bloquea el programa.
            catch(FormatException e)
            {
                Console.Write("Debe ingresar obligatoriamente un número entero.");
            }
            Console.ReadKey();
        }
    }
}

    /*
     using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaExcepcion1
{
   

    Podemos modificar ahora nuestro programa aun más para que no finalice hasta que cargue un valor entero:

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaExcepcion1
{
    class Program
    {
        static void Main(string[] args)
        {
    //añadimos una variable booleana para saber si va a continuar el programa o no
            bool continua;
    //Usamos un do while para que primero lo pruebe el programa y si funciona no vaya al catch y si da error, le decimos que vuelva a funcionar, hasta que se ingrese un valor que funcione.
            do
            {
                try
                {
                    continua = false;
                    Console.Write("Ingrese un valor:");
                    string linea = Console.ReadLine();
                    var num = int.Parse(linea);
                    var cuadrado = num * num;
                    Console.WriteLine($"El cuadrado de {num} es {cuadrado}");
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Debe ingresar obligatoriamente un número entero.");
                    continua = true;
                }
            } while (continua);
            Console.ReadKey();
        }

    }
}


     */

