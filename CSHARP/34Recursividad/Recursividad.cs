﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//Es una técnica de programación que nos permite que un bloque de instrucciones se ejecute n veces
//Hay que tener cuidado al utilizarlo porque hay que ponerle una condición porque sino no se para nunca hasta que se bloquea el ordenador.
namespace _34Recursividad
{
    public class Recursividad
    {

        void Repetir()
        {
            Repetir();
        }

        static void Main(string[] args)
        {
            Recursividad re = new Recursividad();
            re.Repetir();
        }

    }
}