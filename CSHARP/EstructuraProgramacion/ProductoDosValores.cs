﻿//Bibliotecas de palabras especiales que vamos a utilizar
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Estructura de un programa.
/* Estructura general, comienza con una palabra reservada y el nombre del espacio donde va a ir el programa, el espacio comienza con una llave abierta {, dentro se escribe el programa y al final se cierra con otra llave }.*/
namespace SumaProductoNumeros
{
    //Estructura del programa, igual que la anterior, pero hay que seguir el orden y no cambiarlo. En este caso es una clase.
    class Program
    {

        static void Main(string[] args)
        {

            int num1, num2, suma, producto;

            string linea;

            Console.Write("Ingrese primer número:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);

            Console.Write("Ingrese segundo número:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            suma = num1 + num2;
            producto = num1 * num2;

            Console.Write("La suma de los dos valores es:");
            Console.WriteLine(suma);
            Console.Write("El producto de los dos valores es:");
            Console.WriteLine(producto);
            Console.ReadKey();
        }
    }
}

/*Ejercicios:
   1.- Realizar la carga del lado de un cuadrado, mostrar por pantalla el perímetro del mismo (El perímetro de un cuadrado se calcula multiplicando el valor del lado por cuatro).
   2.- Escribir un programa en el cual se ingresen cuatro números, calcular e informar la suma de los dos primeros y el producto del tercero y el cuarto. 
   3.- Realizar un programa que lea cuatro valores numéricos e informar su suma y promedio. 
   4.- Se debe desarrollar un programa que pida el ingreso del precio de un artículo y la cantidad que lleva el cliente. Mostrar lo que debe abonar el comprador. 




1.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerimetroCuadrado
{
    class Program
    {
        static void Main(string[] args)
        {
            int lado,perimetro;
            string linea;
    	    Console.Write("Ingrese el lado del cuadrado:");
    	    linea=Console.ReadLine();
            lado=int.Parse(linea);
    	    perimetro=lado * 4;
    	    Console.Write("El perímetro del cuadrado es:");
    	    Console.Write(perimetro);
            Console.ReadKey();
        }
    }
}




2.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumaProductos4Numeros
{
    class Program
    {
        static void Main(string[] args)
        {
    	    int num1,num2,num3,num4,suma,producto;
            string linea;
    	    Console.Write("Ingrese primer valor:");
            linea=Console.ReadLine();
    	    num1=int.Parse(linea);
    	    Console.Write("Ingrese segundo valor:");
            linea=Console.ReadLine();
    	    num2=int.Parse(linea);
    	    Console.Write("Ingrese tercer valor:");
            linea=Console.ReadLine();
    	    num3=int.Parse(linea);
    	    Console.Write("Ingrese cuarto valor:");
            linea=Console.ReadLine();
    	    num4=int.Parse(linea);
    	    suma=num1 + num2;
    	    producto=num3 * num4;
    	    Console.Write("La suma de los dos primero valores es:");
    	    Console.WriteLine(suma);
    	    Console.Write("El producto del tercer y cuarto valor es:");
    	    Console.Write(producto);
            Console.ReadKey();
        }
    }
}




3.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumaPromedio
{
    class Program
    {
        static void Main(string[] args)
        {
    	    int num1,num2,num3,num4,suma,promedio;
            string linea;
    	    Console.Write("Ingrese primer valor:");
            linea=Console.ReadLine();
    	    num1=int.Parse(linea);
    	    Console.Write("Ingrese segundo valor:");
            linea=Console.ReadLine();
    	    num2=int.Parse(linea);
    	    Console.Write("Ingrese tercer valor:");
            linea=Console.ReadLine();
    	    num3=int.Parse(linea);
    	    Console.Write("Ingrese cuarto valor:");
            linea=Console.ReadLine();
    	    num4=int.Parse(linea);
    	    suma=num1 + num2 + num3 + num4;
    	    promedio=suma/4;
    	    Console.Write("La suma de los cuatro valores es:");
    	    Console.WriteLine(suma);
    	    Console.Write("El promedio es:");
    	    Console.Write(promedio);
            Console.ReadKey();
        }
    }
}




4.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostoCompra
{
    class Program
    {
        static void Main(string[] args)
        {
            int cantidad;
    	    float precio,importe;
            string linea;
    	    Console.Write("Ingrese la cantidad de artículos a llevar:");
            linea=Console.ReadLine();
    	    cantidad=int.Parse(linea);
    	    Console.Write("Ingrese el valor unitario del producto:");
            linea=Console.ReadLine();
    	    precio=float.Parse(linea);
    	    importe=precio * cantidad;
    	    Console.Write("El importe total a pagar es:");
    	    Console.Write(importe);
            Console.ReadKey();
        }
    }
}

*/