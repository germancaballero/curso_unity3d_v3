﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23MatrizArrayParalelo
{
    /* Se tiene la siguiente información:
· Nombres de 4 empleados.
· Ingresos en concepto de sueldo, cobrado por cada empleado, en los últimos 3 meses.
Confeccionar el programa para:

a) Realizar la carga de la información mencionada.
    empleados       sueldos             sueldostot
    Marcos      540 - 540 - 760
    Ana         200 - 220 - 250
    Luis        760 - 760 - 760
    María       605 - 799 - 810
b) Generar un array que contenga el ingreso acumulado en sueldos en los últimos 3 meses para cada empleado.
c) Mostrar por pantalla el total pagado en sueldos a todos los empleados en los últimos 3 meses
d) Obtener el nombre del empleado que tuvo el mayor ingreso acumulado
    */
    class MatrizArrayParalelo
    {
        //Definimos los dos arrays y la matriz para recoger los datos
        private string[] empleados;
        private int[,] sueldos;
        private int[] sueldostot;

        //Creación del método Cargar que recoge los datos del teclado
        public void Cargar()
        {
            //Creamos el array de los empleados
            empleados = new String[4];
            //Creamos la matriz de los sueldos de los empleados
            sueldos = new int[4, 3];
            //Recorremos con el primer for para recorrer las filas
            for (int f = 0; f < empleados.Length; f++)
            {
                Console.Write("Ingrese el nombre del empleado:");
                empleados[f] = Console.ReadLine();
                //Recorremos las columnas(1) para ingresar los sueldos del empleado en el que estamos (fila(0))
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    Console.Write("Ingrese sueldo:");
                    string linea;
                    linea = Console.ReadLine();
                    sueldos[f, c] = int.Parse(linea);
                }
            }
        }

        //Creación del método CalcularSumaSueldos
        public void CalcularSumaSueldos()
        {
            //Creamos el array de los sueldos con el tamaño de los empleados para introducir el total
            sueldostot = new int[4];
            //Recorremos los empleados(filas)
            for (int f = 0; f < sueldos.GetLength(0); f++)
            {
                //Creamos la variable que utilizaremos para la suma de los sueldos.
                int suma = 0;
                //recorremos los sueldos(columnas) del empleado y los vamos sumando en la variable
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    suma = suma + sueldos[f, c];
                }
                //Introducimos el total de la suma en el array paralelo de totales
                sueldostot[f] = suma;
            }
        }

        //Creación del método ImprimirTotalPagado
        public void ImprimirTotalPagado()
        {
            Console.WriteLine("Total de sueldos pagados por empleado.");
            //Recorremos el array de los sueldos totales y los mostramos 
            for (int f = 0; f < sueldostot.Length; f++)
            {
                Console.WriteLine(empleados[f] + " - " + sueldostot[f]);
            }
        }

        //Creamos método EmpleadoMayorSueldo
        public void EmpleadoMayorSueldo()
        {
            //Cargamos los arrays paralelos de sueldos totales y nombres de los empleados del índice primero
            int may = sueldostot[0];
            string nom = empleados[0];
            //Recorremos el array de sueldos totales y buscamos el mayor
            for (int f = 0; f < sueldostot.Length; f++)
            {
                if (sueldostot[f] > may)
                {
                    may = sueldostot[f];
                    nom = empleados[f];
                }
            }
            //Imprimimos el empleado y sueldo mayor
            Console.WriteLine("El empleado con mayor sueldo es " + nom + " que tiene un sueldo de " + may);
        }
        //Inicio del programa
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase para poder llamar y ejecutar los métodos
            MatrizArrayParalelo ma = new MatrizArrayParalelo();
            //Llamamos a Cargar
            ma.Cargar();
            //Llamamos a CalcularSumaSueldos
            ma.CalcularSumaSueldos();
            //Llamamos a ImprimirTotalPagado
            ma.ImprimirTotalPagado();
            //Llamamos a EmpleadoMayorSueldo
            ma.EmpleadoMayorSueldo();
            Console.ReadKey();
        }
    }


    /*
     Se desea saber la temperatura media trimestral de cuatro paises. Para ello se tiene como dato las temperaturas medias mensuales de dichos paises.
Se debe ingresar el nombre del país y seguidamente las tres temperaturas medias mensuales.
Seleccionar las estructuras de datos adecuadas para el almacenamiento de los datos en memoria.
a - Cargar por teclado los nombres de los paises y las temperaturas medias mensuales.
b - Imprimir los nombres de las paises y las temperaturas medias mensuales de las mismas.
c - Calcular la temperatura media trimestral de cada país.
d - Imprimir los nombres de los paises y las temperaturas medias trimestrales.
e - Imprimir el nombre del país con la temperatura media trimestral mayor. 




    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23MatrizArrayParalelo
{
    class MatrizArrayParalelo
    {
        private string[] paises;
        private int[,] tempmen;
        private int[] temptri;

        public void Cargar() 
        {
            paises=new String[4];
            tempmen=new int[4,3];
            for(int f = 0; f < paises.Length; f++)
            {
                Console.Write("Ingrese el nombre del país:");
                paises[f]=Console.ReadLine();
                for(int c = 0; c < tempmen.GetLength(1); c++) 
                {
                    Console.Write("Ingrese temperatura mensual:");
                    string linea = Console.ReadLine();
                    tempmen[f,c]=int.Parse(linea);
                }
            }
        }

        public void ImprimirTempMensuales() 
        {
            for(int f = 0; f < paises.Length; f++)
            {
        	    Console.Write("Pais:" + paises[f]+":");
                for(int c = 0; c < tempmen.GetLength(1); c++)
                {
                    Console.Write(tempmen[f,c]+" ");
                }
                Console.WriteLine();
            }    
        }

        public void CalcularTemperaturaTri()
        {
            temptri = new int[4];
            for (int f = 0; f < tempmen.GetLength(0); f++)
            {
                int suma = 0;
                for (int c = 0; c < tempmen.GetLength(1); c++)
                {
                    suma = suma + tempmen[f,c];
                }
                temptri[f] = suma / 3;
            }
        }

        public void ImprimirTempTrimestrales()
        {
            Console.WriteLine("Temperaturas trimestrales.");
            for(int f = 0; f < paises.Length; f++)
            {
                Console.WriteLine(paises[f]+" "+temptri[f]);
            }
        }

        public void PaisMayorTemperaturaTri() 
        {
            int may=temptri[0];
            string nom=paises[0];
            for(int f = 0; f < paises.Length; f++)
            {
                if (temptri[f] > may) 
                {
                    may=temptri[f];
                    nom=paises[f];
                }
            }
            Console.WriteLine("Pais con temperatura trimestral mayor es "+ nom + " que tiene una temperatura de "+may);        
        }

        static void Main(string[] args)
        {
            MatrizArrayParalelo ma = new MatrizArrayParalelo();
            ma.Cargar();
            ma.ImprimirTempMensuales();
            ma.CalcularTemperaturaTri();
            ma.ImprimirTempTrimestrales();
            ma.PaisMayorTemperaturaTri();
            Console.ReadKey();
        }
    }
}
     */
}
