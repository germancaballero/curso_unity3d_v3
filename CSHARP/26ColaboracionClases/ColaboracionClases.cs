﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Este es el proyecto que contiene las clases
namespace _26ColaboracionClases
{
    //Un banco tiene 3 clientes que pueden hacer depósitos y extracciones. También el banco requiere que al final del día calcule la cantidad de dinero que hay depositado.
    
    
    //Creamos la clase Cliente
        class Cliente
        {
        //declaramos atributos/variables
            private string nombre;
            private int monto;

        //creamos primer método que asigna datos
            public Cliente(string nom)
            {
                nombre = nom;
                monto = 0;
            }
        //creamos segundo método que nos hace una operración para darle un dato a una variable
            public void Depositar(int m)
            {
                monto = monto + m;
            }
        //creamos segundo método que nos hace una operración para darle un dato a una variable
        public void Extraer(int m)
            {
                monto = monto - m;
            }
        //devuelve la variable
            public int RetornarMonto()
            {
                return monto;
            }
        //muestra las dos variables como se pedían
            public void Imprimir()
            {
                Console.WriteLine(nombre + " tiene depositado la suma de " + monto);
            }
        }


    //Creamos la clase Banco
    class Banco
        {
        //declaramos los tres objetos de la clase Cliente que hemos creado y que vamos a manejar
            private Cliente cliente1, cliente2, cliente3;
        //Creamos el primer método que crea los tres objetos y los rellena
            public Banco()
            {
                cliente1 = new Cliente("Juan");
                cliente2 = new Cliente("Ana");
                cliente3 = new Cliente("Pedro");
            }
        //Segundo método, ahora llamamos los métodos de la clase Cliente Depositar y Extraer para modificar el calor de la variable mondo
            public void Operar()
            {
                cliente1.Depositar(100);
                cliente2.Depositar(150);
                cliente3.Depositar(200);
                cliente3.Extraer(150);
            }
        //tercer método
            public void DepositosTotales()
            {
            //Calcula el monto total sumando los montos de cada cliente llamando al método de la clase Clientes
                int t = cliente1.RetornarMonto() +
                        cliente2.RetornarMonto() +
                        cliente3.RetornarMonto();
            // Muestra el total
                Console.WriteLine("El total de dinero en el banco es:" + t);
            //Llama al método imprimir de la clase cliente para mostrar el total de cada cliente
                cliente1.Imprimir();
                cliente2.Imprimir();
                cliente3.Imprimir();
            }
        //Iniciamos el programa
        static void Main(string[] args)
            {
            //Creamos el objeto de la clase Banco para poder acceder también a cliente
                Banco banco1 = new Banco();
                banco1.Operar();
                banco1.DepositosTotales();
                Console.ReadKey();
            }
        }
    */
    /*
     Plantear un programa que permita jugar a los dados. Las reglas de juego son: se tiran tres dados si los tres salen con el mismo valor mostrar un mensaje que "gano", sino "perdió".

    Lo primero que hacemos es identificar las clases:

Podemos identificar la clase Dado y la clase JuegoDeDados.

Luego los atributos y los métodos de cada clase:

Dado		
    atributos
        valor
    métodos
        constructor
        Tirar
        Imprimir
        RetornarValor

JuegoDeDados
    atributos
        3 Dado (3 objetos de la clase Dado)
    métodos
        constructor
        Jugar

Creamos un proyecto llamado: Colaboracion2 y dentro del proyecto creamos dos clases llamadas: Dado y JuegoDeDados.

    ======================================================================================

*/   
    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colaboracion2
{
    class Dado
    {
        private int valor;
        private static Random aleatorio;

        public Dado()
        {
            aleatorio = new Random();
        }

        public void Tirar()
        {
            valor = aleatorio.Next(1, 7);
        }

        public void Imprimir() 
        {
            Console.WriteLine("El valor del dado es:"+valor);
        }

        public int RetornarValor()
        {
            return valor;
        }
    }

    class JuegoDeDados
    {
        private Dado dado1,dado2,dado3;
    
        public JuegoDeDados() 
        {
            dado1=new Dado();
            dado2=new Dado();
            dado3=new Dado();        	
        }
    
        public void Jugar() 
        {
            dado1.Tirar();
            dado1.Imprimir();
            dado2.Tirar();
            dado2.Imprimir();
            dado3.Tirar();
            dado3.Imprimir();
            if (dado1.RetornarValor()==dado2.RetornarValor() && 
                dado1.RetornarValor()==dado3.RetornarValor()) 
            {
                Console.WriteLine("Ganó");
            }
            else
            {
                Console.WriteLine("Perdió");
            }
            Console.ReadKey();
        }
    
        static void Main(string[] args)
        {
            JuegoDeDados j = new JuegoDeDados();
            j.Jugar();
        }
    }
}
     

/*


    Plantear una clase Club y otra clase Socio.
La clase Socio debe tener los siguientes atributos privados: nombre y la antigüedad en el club (en años). En el constructor pedir la carga del nombre y su antigüedad. La clase Club debe tener como atributos 3 objetos de la clase Socio. Definir una responsabilidad para imprimir el nombre del socio con mayor antigüedad en el club. 






    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colaboracion3
{
    class Socio
    {
        private string nombre;
        private int antiguedad;

        public Socio()
        {
            Console.Write("Ingrese el nombre del socio:");
            nombre = Console.ReadLine(); ;
            Console.Write("Ingrese la antiguedad:");
            string linea = Console.ReadLine();
            antiguedad=int.Parse(linea);
        }

        public void Imprimir() 
        {
            Console.WriteLine(nombre+" tiene una antiguedad de "+antiguedad);
        }

        public int RetornarAntiguedad()
        {
            return antiguedad;
        }
    }


    class Club
    {
        private Socio socio1, socio2, socio3;

        public Club() 
        {
            socio1=new Socio();
            socio2=new Socio();
            socio3=new Socio();
        }

        public void MayorAntiguedad()
        {
            Console.Write("Socio con mayor antiguedad:");
            if (socio1.RetornarAntiguedad() > socio2.RetornarAntiguedad() &&
                socio1.RetornarAntiguedad() > socio3.RetornarAntiguedad())
            {
                socio1.Imprimir();
            }
            else
            {
                if (socio2.RetornarAntiguedad() > socio3.RetornarAntiguedad())
                {
                    socio2.Imprimir();
                }
                else
                {
                    socio3.Imprimir();
                }
            }

        }

        static void Main(string[] args)
        {
            Club club1 = new Club();
            club1.MayorAntiguedad();
            Console.ReadKey();
        }
    }
}
     
}*/