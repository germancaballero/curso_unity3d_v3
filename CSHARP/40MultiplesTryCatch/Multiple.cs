﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40MultiplesTryCatch
{
    //Realizar la carga de 2 enteros por teclado, mostrar el resultado de dividir el primero por el segundo.
    class Multiple
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Ingrese primer valor:");
                var num1 = int.Parse(Console.ReadLine());
                Console.Write("Ingrese segundo valor:");
                var num2 = int.Parse(Console.ReadLine());
                var resu = num1 / num2;
                Console.WriteLine($"La división de {num1} respecto a {num2} es {resu}");
            }
            catch (FormatException e)
            {
                Console.Write("Debe ingresar obligatoriamente números enteros.");
            }
            catch (DivideByZeroException e)
            {
                Console.Write("No se puede dividir por cero");
            }
            Console.ReadKey();
        }
    }

    /*
     Declarar un vector de 10 elementos enteros. Permitir que el usuario ingrese un subíndice del vector y nos muestre el contenido de dicha componente. Atrapar las excepciones de fuera de rango del vector y si ingresa un valor no entero.

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaExcepcion4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] vec = { 20, 45, 76, 81, 34, 567, 423, 6, 3, 5 };
            try
            {
                Console.Write("Ingrese un valor entre 0 y 9:");
                var indice = int.Parse(Console.ReadLine());
                Console.WriteLine($"En el vector se almacena en la posición {indice} el valor {vec[indice]}");
            }
            catch (FormatException e)
            {
                Console.Write("Debe ingresar obligatoriamente números enteros.");
            }
            catch (IndexOutOfRangeException e)
            {
                Console.Write("El valor debe estar entre 0 y 9");
            }
            Console.ReadKey();
        }
    }
}

    Veamos con un ejemplo las propiedades fundamentales de la clase Exception.

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaExcepcion5
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Ingrese un valor:");
                string linea = Console.ReadLine();
                var num = int.Parse(linea);
                var cuadrado = num * num;
                Console.WriteLine($"El cuadrado de {num} es {cuadrado}");
            }
            catch (FormatException e)
            {
                Console.WriteLine($"Propiedad Message: {e.Message}\n");
                Console.WriteLine($"Propiedad StackTrade: {e.StackTrace}\n");
                Console.WriteLine($"Propiedad Source: {e.Source}\n");
                Console.WriteLine($"Propiedad TargetSize: {e.TargetSite}");
            }
            Console.ReadKey();
        }
    }
}

    Al provocar el error nos muestra estas propiedades

    Propiedades

    Message: Esta propiedad retorna la descripción de la excepción generada. Es una propiedad de solo lectura.

    StackTrade: Esta propiedad también de solo lectura contiene una cadena que identifica la secuencia de llamadas que desencadenaron la excepción. Esta propiedad es útil durante la depuración, ya que disponemos del número de línea del método que genera la excepción y toda la pila de llamadas a métodos internos.

    Source: Esta propiedad tiene el nombre del ensamblaje, o el objeto, que lanzó la excepción.

    TargetSize: Nos retorna el método que disparó la excepción.

     */
}
