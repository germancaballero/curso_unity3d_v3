﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19ArraysOrdenar
{
    //Se debe crear un vector donde almacenar 5 sueldos. Ordenar el vector sueldos de menor a mayor.
    class ArraysOrdenar
    {
        private int[] sueldos;

        //solicitamos los sueldos y los cargamos en el array
        public void Cargar()
        {
            sueldos = new int[5];
            for (int f = 0; f < sueldos.Length; f++)
            {
                Console.Write("Ingrese el sueldo:");
                string linea = Console.ReadLine();
                sueldos[f] = int.Parse(linea);
            }
        }

        //Método de ordenar los sueldos
        public void Ordenar()
        {
            //Primer for inicializa los cuatro procesos para que abarque todas las combinaciones posible para ordenar, se usan 4 en vez de cinco porque uno es el que movemos y se va intercambiando cuando es mayor.
            for (int k = 0; k < 4; k++)
            {
                //Segundo for interior ordena los sueldos, cada vez pone uno en su sitio
                for (int f = 0; f < 4 - k; f++)
                {
                    if (sueldos[f] > sueldos[f + 1])
                    {
                        int aux;
                        aux = sueldos[f];
                        sueldos[f] = sueldos[f + 1];
                        sueldos[f + 1] = aux;
                    }
                }
            }
        }

        //Mostramos los sueldos ordenados
        public void Imprimir()
        {
            Console.WriteLine("Sueldos ordenados de menor a mayor.");
            //recorremos el array sueldos definitivo para mostrar los sueldos
            for (int f = 0; f < sueldos.Length; f++)
            {
                Console.WriteLine(sueldos[f]);
            }
            Console.ReadKey();
        }

        //Inicio del programa
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase para poder llamar y ejecutar los métodos
            ArraysOrdenar pv = new ArraysOrdenar();
            //Llamamos a Cargar
            pv.Cargar();
            //Llamamos a Ordenar
            pv.Ordenar();
            //Llamamos a Imprimir
            pv.Imprimir();
        }
    }
    /* Definir un array donde almacenar los nombres de 5 paises. Confeccionar el algoritmo de ordenamiento alfabético.
     * 
     * 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19ArraysOrdenar
{
    class ArraysOrdenar
    {
        private string[] paises;

        public void Cargar() 
        {
            paises=new string[5];
            for(int f=0;f < paises.Length;f++) 
            {
                Console.Write("Ingrese el nombre del pais:");
                paises[f]=Console.ReadLine();
            }
        }

        public void Ordenar()
        {
            for (int k = 0; k < 4; k++)
            {
                for (int f = 0; f < 4 - k; f++)
                {
                    if (paises[f].CompareTo(paises[f + 1])>0)
                    {
                        string aux;
                        aux = paises[f];
                        paises[f] = paises[f + 1];
                        paises[f + 1] = aux;
                    }
                }
            }
        }

        public void Imprimir() 
        {
    	    Console.WriteLine("Paises ordenados en forma alfabética:");
            for(int f=0;f < paises.Length;f++) 
            {
                Console.WriteLine(paises[f]);
            }
            Console.ReadKey();
        }


        static void Main(string[] args)
        {
            ArraysOrdenar pv = new ArraysOrdenar();
            pv.Cargar();
            pv.Ordenar();
            pv.Imprimir();
        }
    }
}








Cargar un vector de n elementos de tipo entero. Ordenar posteriormente el vector. 



    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19ArraysOrdenar
{
    class ArraysOrdenar
    {
        private int[] vec;

        public void Cargar() 
        {
            Console.Write("Cuantos elementos tendrá el vector:");
            string linea;
            linea = Console.ReadLine();
            int cant;
            cant=int.Parse(linea);
            vec=new int[cant];
	        for(int f=0;f < vec.Length;f++) 
            {
                Console.Write("Ingrese elemento:");
                linea = Console.ReadLine();
                vec[f]=int.Parse(linea);
	        }
	    }

        public void Ordenar()
        {
            for (int k = 0; k < vec.Length; k++)
            {
                for (int f = 0; f < vec.Length - 1 - k; f++)
                {
                    if (vec[f] > vec[f + 1])
                    {
                        int aux;
                        aux = vec[f];
                        vec[f] = vec[f + 1];
                        vec[f + 1] = aux;
                    }
                }
            }
        }

        public void Imprimir() 
        {
	    Console.WriteLine("Vector ordenados de menor a mayor.");
            for(int f=0;f < vec.Length;f++) 
            {
                Console.WriteLine(vec[f]);
            }
            Console.ReadKey();
        }

        
        static void Main(string[] args)
        {
            ArraysOrdenar pv = new ArraysOrdenar();
            pv.Cargar();
            pv.Ordenar();
            pv.Imprimir();
        }
    }
}


     */
}
