﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _38Propiedades
{
    //Plantear una clase llamada Triangulo. Por defecto el valor de cada lado del triángulo es 5. Calcular el perímetro.
    class Propiedades
    {
        //Creamos las propiedades y les asigmnamos un valor
        public int Lado1 { get; set; } = 5;
        public int Lado2 { get; set; } = 5;
        public int Lado3 { get; set; } = 5;

        public int RetornarPerimetro()
        {
            //Devolvemos el resultado de una operación con las propiedades
            return Lado1 + Lado2 + Lado3;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Creamos un objeto de propiedades y le asignamos valores a las propiedades
            Propiedades triangulo1 = new Propiedades { Lado1 = 10, Lado2 = 20, Lado3 = 30 };
            //Mostramos el resultado de la operación que pedimos
            Console.WriteLine("El perímetro es: " + triangulo1.RetornarPerimetro());
            //Creamos un objeto de propiedades y le asignamos valor a una sola propiedad
            Propiedades triangulo2 = new Propiedades { Lado3 = 30 };
            Console.WriteLine("El perímetro es: " + triangulo2.RetornarPerimetro());
            Console.ReadKey();
        }
    }
}
