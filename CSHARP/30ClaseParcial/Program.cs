﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30ClaseParcial
{
    //Plantear una clase Rectangulo, definir dos propiedades: Lado1 y Lado2. Definir dos métodos RetornarSuperficie y RetornarPerimetro. Dividir la clase en dos archivos utilizando el concepto de "partial class".
    class Program
    {
        //Creamo el programa de inicio
        static void Main(string[] args)
        {
            //Creamos el objeto para llamar a la clase Rectangulo
            Rectangulo rectangulo1 = new Rectangulo();
            //Damos datos a las propiedades del Archivo1 del rectángulo
            rectangulo1.Lado1 = 5;
            rectangulo1.Lado2 = 10;
            //Mostramos llamando al Archivo2 que hace las operaciones
            Console.WriteLine("La superficie del rectángulo es:" +
                                rectangulo1.RetornarSuperficie());
            Console.WriteLine("El perímetro del rectángulo es:" +
                                rectangulo1.RetornarPerimetro());
            Console.ReadKey();
        }
    }
}