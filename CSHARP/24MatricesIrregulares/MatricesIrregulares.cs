﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24MatricesIrregulares
{
    //Confeccionaremos un programa que permita crear una matriz irregular y luego imprimir la matriz en forma completa. 
    class MatricesIrregulares
    {
        //Definimos una matriz irregula, los índices de líneas y columnas van independientes entre corchetes
        private int[][] mat;

        //Creamos el método de carga de datos
        public void Cargar()
        {
            //Pedimos el número de filas, ya que es lo primero que debe asignarse
            Console.Write("Cuantas fila tiene la matriz:");
            string linea = Console.ReadLine();
            int filas = int.Parse(linea);
            //Ahora creamos las filas de la matriz
            mat = new int[filas][];
            //Utilizamos el for para preguntar poe el número de columnas de cada fila
            for (int f = 0; f < mat.Length; f++)
            {
                Console.Write("Cuantas elementos tiene la fila " + f + ":");
                linea = Console.ReadLine();
                int elementos = int.Parse(linea);
                //Le asignamos el número de columnas que tiene cada fila
                mat[f] = new int[elementos];
                //En este for se ingresan los datos de las columnas por cada fila
                for (int c = 0; c < mat[f].Length; c++)
                {
                    Console.Write("Ingrese componente:");
                    linea = Console.ReadLine();
                    mat[f][c] = int.Parse(linea);
                }
            }
        }

        //Creación del método Imprimir
        public void Imprimir()
        {
            //Recorremos cada fila
            for (int f = 0; f < mat.Length; f++)
            {
                //Recorremos cada columna perteciente a cada fila
                for (int c = 0; c < mat[f].Length; c++)
                {
                    //Lo mostramos
                    Console.Write(mat[f][c] + " ");
                }
                //Hacemos el salto de línea para que se muestre bien la matriz
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        //Inicio del programa
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase para poder llamar y ejecutar los métodos
            MatricesIrregulares ma = new MatricesIrregulares();
            //Llamamos a Cargar
            ma.Cargar();
            //Llamamos a Imprimir
            ma.Imprimir();
        }
    }

    /*
     Confeccionar una clase para administrar una matriz irregular de 5 filas y 1 columna la primer fila, 2 columnas la segunda fila y así sucesivamente hasta 5 columnas la última fila (crearla sin la intervención del operador)
Realizar la carga por teclado e imprimir posteriormente. 

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24MatricesIrregulares
{
    class MatricesIrregulares
    {
        private int[][] mat;

        public void Cargar() 
        {
            mat=new int[5][];
            for(int f = 0; f < mat.Length; f++) 
            {
                mat[f]=new int[f+1];            
                for(int c = 0; c < mat[f].Length; c++)
                {
                    Console.Write("Ingrese componente:");
                    string linea = Console.ReadLine();
                    mat[f][c]=int.Parse(linea);
                }
            }
        }

        public void Imprimir() 
        {
            for(int f = 0; f < mat.Length; f++)
            {
                for(int c = 0; c < mat[f].Length; c++)
                {
                    Console.Write(mat[f][c]+" ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            MatricesIrregulares ma = new MatricesIrregulares();
            ma.Cargar();
            ma.Imprimir();
        }
    }
}



    Confeccionar una clase para administrar los días que han faltado los 3 empleados de una empresa.
Definir un vector de 3 elementos de tipo string para cargar los nombres y una matriz irregular para cargar los días que han faltado cada empleado (cargar el número de día que faltó)
Cada fila de la matriz representan los días de cada empleado.
Mostrar los empleados con la cantidad de inasistencias.
Cuál empleado faltó menos días. 

    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24MatricesIrregulares
{
    class MatricesIrregulares
    {
        private string[] nombres;
        private int[][] dias;

        public void Cargar() 
        {
            nombres=new string[3];
            dias=new int[3][];
            for(int f = 0; f < nombres.Length; f++) 
            {
                Console.Write("Ingrese el nombre del empleado:");
                nombres[f]=Console.ReadLine();
                Console.Write("Cuantas días faltó el empleado:");
                string linea = Console.ReadLine();
                int faltas=int.Parse(linea);
                dias[f]=new int[faltas];            
                for(int c = 0; c < dias[f].Length; c++)
                {
                    Console.Write("Ingrese nro de día:");
                    linea = Console.ReadLine();
                    dias[f][c]=int.Parse(linea);
                }
            }
        }

        public void Inasistencias() 
        {
            for(int f = 0; f < nombres.Length; f++) 
            {
                Console.WriteLine(nombres[f] + " faltó " + dias[f].Length + " días");
            }
        }

        public void EmpleadoMensosFaltas() 
        {
            int faltas=dias[0].Length;
            string nom=nombres[0];
            for(int f = 1; f < dias.Length; f++) 
            {
                if (dias[f].Length < faltas) 
                {
                    faltas=dias[f].Length;
                    nom=nombres[f];
                }
            }
            Console.WriteLine("El empleado que faltó menos es "+nom+" con "+faltas+" faltas.");
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            MatricesIrregulares ma = new MatricesIrregulares();
            ma.Cargar();
            ma.Inasistencias();
            ma.EmpleadoMensosFaltas();
        }
    }
}
     */
}
