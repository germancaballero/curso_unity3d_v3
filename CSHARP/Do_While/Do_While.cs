﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Do_While
{
    //Escribir un programa que solicite la carga de un número entre 0 y 999, y nos muestre un mensaje de cuántos dígitos tiene el mismo. Finalizar el programa cuando se cargue el valor 0.
    class Do_While
    {
        static void Main(string[] args)
        {
            //Declaramos primer valor integer.
            int valor;
            //Declaramos primer valor string.
            string linea;
            //Iniciamos el programa Do While
            do
            {
                //Ejecutamos las condiciones que vamos a comparar para obtener el verdadero o falso para ejecutar el Do While.
                //Pedimos el dato al usuario, limitando entre que números y como terminar el bucle.
                Console.Write("Ingrese un valor entre 0 y 999 (0 finaliza):");
                linea = Console.ReadLine();
                valor = int.Parse(linea);
                //Establecemos la primera condición, si el valor es mayor o igual de 100 es que tiene 3 dígitos.
                //Comparadores   " && es y  "   " || es o  "  Que sea menor que 1000.
                if (valor >= 100 && valor < 1000)
                {
                    //Lo escribimos.
                    Console.WriteLine("Tiene 3 dígitos.");
                }
                //Si no cumple dicha condición, entra.
                else
                {
                    //Le ponemos otra condición, para filtrar los de dos dígitos.
                    if (valor >= 10 && valor < 1000)
                    {
                        Console.WriteLine("Tiene 2 dígitos.");
                    }
                    //Ya solo nos quedan los de un dígito
                    else
                    {
                        if (valor < 10)
                        {
                            Console.WriteLine("Tiene 1 dígito.");
                        }

                    }
                }
                //Una vez mostrado los dígitos del número introducido, incluido el 0, entonces ahora es cuando comprobamos que si es distinto de cero se vuel a iniciar el ciclo, sino se sale.
            } while (valor != 0);
            Console.ReadKey();
        }
    }
}

/*
 1.- Escribir un programa que solicite la carga de números por teclado, obtener su promedio. Finalizar la carga de valores cuando se cargue el valor 0.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaDoWhile2
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma,cant,valor,promedio;
            string linea;
            suma=0;
            cant=0;
            do {
                Console.Write("Ingrese un valor (0 para finalizar):");
                linea = Console.ReadLine();
                valor=int.Parse(linea);
                if (valor!=0) {
                    suma=suma+valor;
                    cant++;
                }
            } while (valor!=0);
            if (cant!=0) {
                promedio=suma/cant;
                Console.Write("El promedio de los valores ingresados es:");
                Console.Write(promedio);
            } else {
                Console.Write("No se ingresaron valores.");
            }
            Console.ReadLine();
        }
    }
}

2.- Realizar un programa que permita ingresar el peso (en kilogramos) de piezas. El proceso termina cuando ingresamos el valor 0. Se debe informar:
a) Cuántas piezas tienen un peso entre 9.8 Kg. y 10.2 Kg.?, cuántas con más de 10.2 Kg.? y cuántas con menos de 9.8 Kg.?
b) La cantidad total de piezas procesadas. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaDoWhile3
{
    class Program
    {
        static void Main(string[] args)
        {
            int cant1,cant2,cant3,suma;
            float peso;
            string linea;
            cant1=0;
            cant2=0;
            cant3=0;
            do {
                Console.Write("Ingrese el peso de la pieza (0 pera finalizar):");
                linea = Console.ReadLine();
                peso=float.Parse(linea);
                if (peso>10.2) 
                {
                    cant1++;
                }
                else 
                {
                    if (peso>=9.8)
                    {
                        cant2++;
                    }
                    else 
                    {
                        if (peso>0) 
                        {
                            cant3++;
                        }
                    }
                }
            } while(peso!=0);
            suma=cant1+cant2+cant3;
            Console.Write("Piezas aptas:");
            Console.WriteLine(cant2);
            Console.Write("Piezas con un peso superior a 10.2:");
            Console.WriteLine(cant1);
            Console.Write("Piezas con un peso inferior a 9.8:");
            Console.WriteLine(cant3);
            Console.Write("Cantidad de piezas procesadas:");
            Console.WriteLine(suma);
            Console.ReadLine();
        }
    }
}

3.- Realizar un programa que acumule (sume) valores ingresados por teclado hasta ingresar el 9999 (no sumar dicho valor, indica que ha finalizado la carga). Imprimir el valor acumulado e informar si dicho valor es cero, mayor a cero o menor a cero. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaDoWhile4
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma,valor;
            string linea;
            suma=0;
            do {
                Console.Write("Ingrese un valor:");
                linea = Console.ReadLine();
                valor=int.Parse(linea);
                if (valor!=9999) 
                {
                    suma=suma+valor;
                }
            }while (valor!=9999);
            Console.Write("El valor acumulado es ");
            Console.WriteLine(suma);
            if (suma==0) 
            {
                Console.WriteLine("El valor acumulado es cero.");
            }
            else 
            {
                if (suma>0) 
                {
                    Console.WriteLine("El valor acumulado es positivo.");
                } 
                else 
                {
                    Console.WriteLine("El valor acumulado es negativo");
                }
            }
            Console.ReadKey();
        }
    }
}

4.- En un banco se procesan datos de las cuentas corrientes de sus clientes. De cada cuenta corriente se conoce: número de cuenta y saldo actual. El ingreso de datos debe finalizar al ingresar un valor negativo en el número de cuenta.
Se pide confeccionar un programa que lea los datos de las cuentas corrientes e informe:
a)De cada cuenta: número de cuenta y estado de la cuenta según su saldo, sabiendo que:

Estado de la cuenta	'Acreedor' si el saldo es >0.
			'Deudor' si el saldo es <0.
			'Nulo' si el saldo es =0.

b) La suma total de los saldos acreedores. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaDoWhile5
{
    class Program
    {
        static void Main(string[] args)
        {
            int cuenta;
            float saldo,suma;
            string linea;
            suma=0;
            do {
                Console.Write("Ingrese número de cuenta:");
                linea = Console.ReadLine();
                cuenta=int.Parse(linea);
                if (cuenta>=0) 
                {
                    Console.Write("Ingrese saldo:");
                    linea = Console.ReadLine();
                    saldo=float.Parse(linea);
                    if (saldo>0) 
                    {
                        Console.WriteLine("Saldo Acreedor.");
                        suma=suma+saldo;
                    } 
                    else 
                    {
                        if (saldo<0) 
                        {
                            Console.WriteLine("Saldo Deudor.");
                        } 
                        else 
                        {
                            Console.WriteLine("Saldo Nulo.");
                        }
                    }
                }
            } while(cuenta>=0);
            Console.Write("Total de saldos Acreedores:");
            Console.Write(suma);
            Console.ReadKey();
        }
    }
}
 
 
 */
