﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27Propiedad
{
    //El problema era : Un banco tiene 3 clientes que pueden hacer depósitos y extracciones. También el banco requiere que al final del día calcule la cantidad de dinero que hay depositada.
    class Cliente
    {
        //Declaramos las variables
        private string nombre;
        private int monto;

        //Creamos la propiedad que es una forma de acceder al contenido de un atributo, tanto para consultar su valor como modificarlo.
        public string Nombre
        {
            //Modificamos el valor
            set
            {
                nombre = value;
            }
            //cogemos su valor
            get
            {
                return nombre;
            }
        }

        public int Monto
        {
            //Modificamos el valor
            set
            {
                monto = value;
            }
            //cogemos su valor
            get
            {
                return monto;
            }
        }

        //Creamos el método que mostrará los datos
        public void Imprimir()
        {
            Console.WriteLine(Nombre + " tiene depositado la suma de " + Monto);
        }
    }

    //Creamos la clase Cliente
    class Banco
    {
        //Declaramos lobjetos de la clase Clientes
        private Cliente cliente1, cliente2, cliente3;
        //Creamos el método para introducir los datos
        public Banco()
        {
            //Creamos el primer objeto
            cliente1 = new Cliente();
            //Le damos los valores a las propiedades
            cliente1.Nombre = "Juan";
            cliente1.Monto = 0;
            cliente2 = new Cliente();
            cliente2.Nombre = "Ana";
            cliente2.Monto = 0;
            cliente3 = new Cliente();
            cliente3.Nombre = "Pedro";
            cliente3.Monto = 0;
        }
        //Creamos el método para recuperar ese datos y operar con ellos
        public void Operar()
        {
            cliente1.Monto = cliente1.Monto + 100;
            cliente2.Monto = cliente2.Monto + 150;
            cliente3.Monto = cliente3.Monto + 200;
        }
        //Creamos el método donde resolvemos el problema, mostramos los datos
        public void DepositosTotales()
        {
            int t = cliente1.Monto + cliente2.Monto + cliente3.Monto;
            Console.WriteLine("El total de dinero en el banco es:" + t);
            cliente1.Imprimir();
            cliente2.Imprimir();
            cliente3.Imprimir();
        }
        //Iniciamos el programa
        static void Main(string[] args)
        {
            //Creamos el objeto para llamar a la clase banco que luego maneja las propiedades
            Banco banco1 = new Banco();
            banco1.Operar();
            banco1.DepositosTotales();
            Console.ReadKey();
        }
    }

    /*
     Plantear un programa que permita jugar a los dados. Las reglas de juego son: se tiran tres dados si los tres salen con el mismo valor mostrar un mensaje que "gano", sino "perdió".
     

*/
    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Propiedades2
{
        class Dado
        {
            private int valor;

            public int Valor
            {
                get
                {
                    return valor;
                }
                private set
                {
                    valor = value;
                }
            }

            private static Random aleatorio;

            public Dado()
            {
                aleatorio = new Random();
            }

            public void Tirar()
            {
                Valor = aleatorio.Next(1, 7);
            }

            public void Imprimir()
            {
                Console.WriteLine("El valor del dado es:" + Valor);
            }
        }

        class JuegoDeDados
        {
            private Dado dado1, dado2, dado3;

            public JuegoDeDados()
            {
                dado1 = new Dado();
                dado2 = new Dado();
                dado3 = new Dado();
            }

            public void Jugar()
            {
                dado1.Tirar();
                dado1.Imprimir();
                dado2.Tirar();
                dado2.Imprimir();
                dado3.Tirar();
                dado3.Imprimir();
                if (dado1.Valor == dado2.Valor && dado1.Valor == dado3.Valor)
                {
                    Console.WriteLine("Ganó");
                }
                else
                {
                    Console.WriteLine("Perdió");
                }
                Console.ReadKey();
            }

            static void Main(string[] args)
            {
                JuegoDeDados j = new JuegoDeDados();
                j.Jugar();
            }
        }

}
/*


    Plantear una clase Club y otra clase Socio.
La clase Socio debe tener los siguientes atributos privados: nombre y la antigüedad en el club (en años) Definir dos propiedades para poder acceder al nombre y la antigüedad del socio(no permitir cargar un valor negativo en la antigüedad). La clase Club debe tener como atributos 3 objetos de la clase Socio. Definir una responsabilidad para imprimir el nombre del socio con mayor antigüedad en el club. 



    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Propiedades3
{
    class Socio
    {
        private string nombre;
        private int antiguedad;

        public string Nombre
        {
            set
            {
                nombre = value;
            }
            get
            {
                return nombre;
            }
        }

        public int Antiguedad
        {
            set
            {
                if (value >= 0)
                {
                    antiguedad = value;
                }
                else
                {
                    Console.Write("No se puede asignar aun valor negativo a la antiguedad");
                }
            }
            get
            {
                return antiguedad;
            }
        }
    }


    class Club
    {
        private Socio socio1, socio2, socio3;

        public Club()
        {
            socio1 = new Socio();
            socio1.Nombre = "Juan";
            socio1.Antiguedad = 7;
            socio2 = new Socio();
            socio2.Nombre = "Ana";
            socio2.Antiguedad = 3;
            socio3 = new Socio();
            socio3.Nombre = "Martin";
            socio3.Antiguedad = 25;
        }

        public void MayorAntiguedad()
        {
            if (socio1.Antiguedad > socio2.Antiguedad &&
                socio1.Antiguedad > socio3.Antiguedad)
            {
                Console.WriteLine("Socio com mayor antiguedad:"+socio1.Nombre);
            }
            else
            {
                if (socio2.Antiguedad > socio3.Antiguedad)
                {
                    Console.WriteLine("Socio com mayor antiguedad:" + socio2.Nombre);    
                }
                else
                {
                    Console.WriteLine("Socio com mayor antiguedad:" + socio3.Nombre);
                }
            }
        }

        static void Main(string[] args)
        {
            Club club1 = new Club();
            club1.MayorAntiguedad();
            Console.ReadKey();
        }
    }
}
    
}
 */

