﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraSigueObjeto : MonoBehaviour
{
    // Start is called before the first frame update
    //Declaramos la variable publica que coge al objeto.
    public Vector3 offset;
    //Declaramos la variable private que coge las carasterísticas del transform del objeto.
    private Transform target;
    //Declaramos la variable de la distancia entre la cámara y el personaje. Le añadimos un rango para que no se nos vaya hastta el infinito.
    [Range (0,1)]public float lerpValue;

    void Start()
    {
        //Cargar el transform para utilizarlo después.
        target = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        //Asignamos la posición de la cámara al jugador, para que le siga.
        transform.position = Vector3.Lerp(transform.position, target.position + offset, lerpValue);
        //Para que mire al jugador.
        transform.LookAt(target);
    }
}
