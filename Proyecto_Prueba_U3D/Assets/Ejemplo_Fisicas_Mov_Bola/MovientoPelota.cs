﻿using UnityEngine;

public class MovientoPelota : MonoBehaviour
{
    public float velocidad = 2f; // Sólo es un valor por defecto, se pondrá cambiar en Unity
    int chocquesPuerta;
    public int cantidadCubos = 70000;

    public GameObject cubo;
    Animator anim;

    // Start is called before the first frame update
    private void Start()
    {
        Debug.Log(transform.gameObject.name);
        // transform.position.x = 10;
        // DestroyObject(gameObject);
        // transform.position = new Vector3(0, 5, 0);
        chocquesPuerta = 3;
        // Time.timeScale = 0.2f;
        for (int i = 0; i < cantidadCubos; i = i + 1) {
            GameObject.Instantiate(cubo, new Vector3((float)i / (cantidadCubos/10), 10, 0), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update() {
        /* Provoca una excepción:     GameObject vacio = null;
         vacio.transform.position = Vector3.zero;
        */

        bool siFlechaIzquierdaPulsada = Input.GetKey(KeyCode.LeftArrow);
        // float desplazamientoX = 2f * Time.deltaTime;
        Vector3 desplazamiento = new Vector3(velocidad, 0, 0) * Time.deltaTime;

        Vector3 posicion_izquierda = new Vector3(-3, transform.position.y, 0);
        if (gameObject.transform.position.x <= posicion_izquierda.x) {
            gameObject.transform.position = posicion_izquierda;
            siFlechaIzquierdaPulsada = false;
        }

        if (siFlechaIzquierdaPulsada) {

            transform.position -= desplazamiento;
            // transform.position = transform.position + desplazamiento;
        }
        bool siFlechaDerechaPulsada = Input.GetKey(KeyCode.RightArrow);

        Vector3 posicion_derecha = new Vector3(+3, transform.position.y, 0);
        if (gameObject.transform.position.x >= posicion_derecha.x) {
            gameObject.transform.position = posicion_derecha;
            siFlechaDerechaPulsada = false;
        }

        if (siFlechaDerechaPulsada) {

            transform.position += desplazamiento;
            //transform.position = transform.position - desplazamiento;
        }


        Debug.Log("Time = " + Time.time
            + ", Delta time = " + Time.deltaTime + " segundos.");

    }
  
    /*
    void OnTriggerEnter(Collider collider) {
        Debug.Log("OnTriggerEnter");
    }*/
    void OnCollisionEnter(Collision collision) {

        Debug.Log("OnCollisionEnter: " + collision.contacts[0].point.ToString()
            + " Nombre: " +collision.gameObject.name);

        if (collision.gameObject.name == "Objeto_Cubo") {
            chocquesPuerta--;
            if (chocquesPuerta == 0) {

                collision.gameObject.SetActive(false);
            }
        }
    }
}
