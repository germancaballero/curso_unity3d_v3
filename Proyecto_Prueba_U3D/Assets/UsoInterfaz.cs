﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsoInterfaz : MonoBehaviour
{
    public Toggle toggle;
    public AudioSource audioMusica;

    // Start is called before the first frame update
    void Start()
    {
        toggle.transform.GetChild(1).GetComponent<Text>().text = "SONIDO";
        toggle.isOn = false;
        audioMusica.mute = true;
        toggle.onValueChanged.AddListener(CambioToggle);
    }
    public void CambioToggle(bool nuevoValor) {
        audioMusica.mute = ! nuevoValor;
    }
}
