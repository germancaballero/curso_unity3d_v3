﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugadora : MonoBehaviour
{
    public GameObject spriteJug;
    public GameObject suelo;
    public GameObject posicionRayCast;
    public Camera camara;
    Vector3 velocidad;
    public float gravedad = -9.8f;
    private float posSueloY;
    private Vector3 distanciaCamara;

    // Start is called before the first frame update
    void Start()
    {
        posSueloY = suelo.transform.position.y + suelo.transform.localScale.y / 2f;
        distanciaCamara = transform.position - camara.transform.position;
    }
    const float VELOCIDAD_X = 10;

    // Update is called once per frame
    // Evento que se llama por cada visualización frame de la gráfica
    void Update() { 

        velocidad.x = 0;

        if (Input.GetKey(KeyCode.LeftArrow) == true) {
            velocidad.x = -VELOCIDAD_X;
        } 
        if (Input.GetKey(KeyCode.RightArrow) == true) {
            velocidad.x = +VELOCIDAD_X;
        }
        if (Input.GetKey(KeyCode.Space) && transform.position.y <= posSueloY) {
            velocidad.y = 15;
        }
        velocidad.y += gravedad * Time.deltaTime;

        transform.position += velocidad * Time.deltaTime;

        spriteJug.GetComponent<Animator>().SetFloat("velocidad_x", Mathf.Abs(velocidad.x));
        if (velocidad.x > 0)
            spriteJug.transform.localScale = Vector3.one;
        if (velocidad.x < 0)
            spriteJug.transform.localScale = new Vector3(-1, 1, 1);

        if (transform.position.y < posSueloY) {
            gravedad = 0;
            velocidad.y = 0;
            transform.position = new Vector3(transform.position.x, posSueloY, transform.position.z);
        }
        else {
            gravedad = -30f;
        }
        /*if (velocidad.x < 0 || velocidad.x > 0)   Alternativa
            spriteJug.GetComponent<Animator>().SetFloat("velocidad_x", 1);*/
        Vector3 posicionFinalCam = new Vector3(transform.position.x + distanciaCamara.x, transform.position.y - distanciaCamara.y, camara.transform.position.z);

        camara.transform.position = Vector3.Lerp( camara.transform.position, posicionFinalCam, 0.05f) ;
    }
    // Evento que se llama por cada gestión de físicas de la gráfica
    private void FixedUpdate() {
        Vector3 direccion = velocidad.x < 0 ? Vector3.left : Vector3.right;
        RaycastHit hit;
        int layerMask = 1 << 5 ;
        Debug.DrawRay(transform.position, transform.TransformDirection(direccion) * 1000, Color.yellow);

        RaycastHit2D rayCast = Physics2D.Raycast(transform.position, transform.TransformDirection(direccion));
        if (rayCast) {
            Debug.Log("Did Hit");
        }


        Vector3 direccion2 = spriteJug.transform.localScale.x < 0 ? Vector3.left : Vector3.right;
        /*if (Physics.Raycast(transform.position, transform.TransformDirection(direccion2), out hit, 20f, layerMask)) {
            Debug.Log("Did Hit");
        }*/

        RaycastHit2D rc2 = Physics2D.Raycast(posicionRayCast.transform.position, posicionRayCast.transform.TransformDirection(direccion2));

        Debug.DrawRay(posicionRayCast.transform.position, posicionRayCast.transform.TransformDirection(direccion2) * 1000, Color.red);

        if (rc2) {
            Debug.Log("Did Hit 2");
            GameObject bola = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            bola.transform.position = rc2.point;
        }
        /*else {
            Debug.DrawRay(transform.position, transform.TransformDirection(direccion) * 1000, Color.white);
            Debug.Log("Did not Hit");
        }*/
    }

}
