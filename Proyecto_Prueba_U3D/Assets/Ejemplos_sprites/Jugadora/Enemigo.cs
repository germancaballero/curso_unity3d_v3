﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    GameObject jugadora;

    private void Start() {
        // SÓLO LLAMAR EN START, NUNCA EN UPDATE, CON MESURA
        jugadora = GameObject.Find("Sprite_chica_jug");
    }
    private void OnCollisionEnter(Collision other) {
        GameObject cilindro =  GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        cilindro.transform.position = other.contacts[0].point;

        // jugadora.transform.parent.LookAt(other.contacts[0].point);

        Destroy(this.gameObject);
    }
}
