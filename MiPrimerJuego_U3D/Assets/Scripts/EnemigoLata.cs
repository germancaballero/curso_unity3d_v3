﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoLata : MonoBehaviour
{
    public ControladorJuego ctrlJuego;

    public float velocidad; // Por defecto, cero
    GameObject jugador;

    // Start se llama la primera vez, primer frame
    void Start()
    {
        // Buscamos un GameObject por su nombre, sólo hacer en Start()
        jugador = GameObject.Find("Jugador-Caballito");
        ctrlJuego = GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;

        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position
            + movAbajo;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnTriggerEnter2D(collision.gameObject.GetComponent<Collider2D>());
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Colisionado: " + collision.gameObject.name);

        if (collision.gameObject.name.ToLower().Contains("final"))
        {
            ctrlJuego.CuandoPerdemosEnemigo();
            this.enabled = false;
        }
        else if (collision.gameObject.name.ToLower().Contains("caballito"))
        {
            ctrlJuego.CuandoCapturamosEnemigo();
            Destroy(this.gameObject);
        }

    }
}
