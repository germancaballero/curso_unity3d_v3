﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerseguidorCamara : MonoBehaviour
{
    public GameObject jugador;

    // Update is called once per frame
    void Update()
    {
        float x = Mathf.Lerp(this.transform.position.x, jugador.transform.position.x, .1f);
        if (x > 2f) x = 2f; else if (x < -2f) x = -2f;

        this.transform.position = new Vector3(x, this.transform.position.y, this.transform.position.z);
    }
}
