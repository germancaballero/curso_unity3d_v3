using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigosAsteroides : MonoBehaviour
{

    public int da�o = 5 ;
    public GameObject choque ;
    Vector3 direccion;
    GameObject choqueInstancia;

    private void OnCollisionEnter(Collision colision) {

  
        ContactPoint[] puntosDeContacto = colision.contacts;

        Vector3 puntoDeChoque = puntosDeContacto[0].point ;

        choqueInstancia = Instantiate(choque, puntoDeChoque, Quaternion.identity);
          
        if (puntosDeContacto[0].otherCollider.tag == "Asteroide") {

            Vector3 velocidadAsteroide2 = puntosDeContacto[0].otherCollider.GetComponent<Rigidbody>().velocity;
            Vector3 velocidadAsteroide1 = this.GetComponent<Rigidbody>().velocity;
            direccion = velocidadAsteroide2 + velocidadAsteroide1 / 2;
            Destroy(choqueInstancia, 4);
        } 
    }

    public void Update() {

        if (choqueInstancia != null) {
            choqueInstancia.transform.position += direccion * Time.deltaTime; 
        }

        if (transform.position.z < -10 ) {
            Destroy(this.gameObject);
        }
    }

    public void FixedUpdate () { 

        if (transform.position.z < -10) {
            Destroy(this.gameObject);
        }
    } 

}
