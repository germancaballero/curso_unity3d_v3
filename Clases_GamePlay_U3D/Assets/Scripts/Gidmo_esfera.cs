using System.Collections ;
using System.Collections.Generic ;
using UnityEngine ;

public class Gidmo_esfera : MonoBehaviour {

    public Color color = Color.yellow ;
    public float tamaNo = 1 ;

    private void OnDrawGizmos() {

        Gizmos.color = color ;
        Gizmos.DrawWireSphere ( transform.position, tamaNo ) ;
            
    }

}
