using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoLine : MonoBehaviour {

    public Color color = Color.yellow;
    public Transform objetoA ;
    public Transform objetoB ;

    private void OnDrawGizmos() {
        Gizmos.color = color ;
        Gizmos.DrawLine( objetoA.position , objetoB.position ) ; 
    }
}
