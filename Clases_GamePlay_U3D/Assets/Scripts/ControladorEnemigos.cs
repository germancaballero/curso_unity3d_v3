using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorEnemigos : MonoBehaviour
{
    private ControladorJuego controladorJuego ;

    public GameObject enemigoFuente;
    public GameObject enemigoGrupo;

    public List<GameObject> enemigos = new List<GameObject>();

    public void Inicio()
    {
        controladorJuego = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorJuego>();
    }
    public void aNadirEnemigo ()
    {
        GameObject e = Instantiate(enemigoFuente);
        enemigos.Add(e) ;
        e.transform.parent = enemigoGrupo.transform;

    }
    public void subirVida()
    {

    }

    public void Actualizar()
    {
        controladorJuego.controladorPlayers.getPlayerB();
    }
    public void distanciaEnemigos( )
    {

    }
}
