using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuegoNaves : MonoBehaviour {
    
    [Header("EspacioJuego")] 
    public Transform limiteD ;
    public Transform limiteI ;
    public Transform profundidadLejos ;

    [Header("objetos")]
    public GameObject nave;
    public Transform cursor;
    public Transform posicionNave;

    public GameObject[] asteroidesFuente;

    [Header("UI")]
    public Slider barraVida;
    public Slider barraVida2;

    [Header("configuracion")]
    public float velocidad = 5 ;
    public float aceleracion = 5;
    public float intervalo = 5;

    private Animator animacion;

    private void Start()
    {
        animacion = nave.GetComponent<Animator>() ; 
        instanciarAsteroide(); 
    }

    public void cambiarVida(int vida) { 

        barraVida.value = vida / 20f ;
        StartCoroutine( "Vida2", vida / 20f); 
    }

    float tiempo2;
    IEnumerator Vida2 ( float vida2 ) { 

        yield return new WaitForSeconds(.5f) ; 

        barraVida2.value = vida2 ;
        
    }


    // Update is called once per frame  
    float tiempo = 0;
    void Update() {
          
        tiempo = tiempo + Time.deltaTime ;
        if (tiempo > intervalo) {
            instanciarAsteroide() ;
            tiempo = 0;
        }


        int faseDeMovimiento = movimientoTouchEnPantalla();
        if ( faseDeMovimiento == 1  & cursor.position.x < limiteD.position.x ) { 
            cursor.position = cursor.position + new Vector3( Time.deltaTime * velocidad, 0 , 0 ) ; 
        }
        if ( faseDeMovimiento == -1  & cursor.position.x > limiteI.position.x ) {
            cursor.position = cursor.position + new Vector3( Time.deltaTime * -velocidad, 0, 0);
        } 
        posicionNave.position = Vector3.Lerp ( posicionNave.position, cursor.position , Time.deltaTime * aceleracion ) ;

        Vector3 distancia = cursor.position - posicionNave.position ;
        animacion.SetFloat( "BlendGiro", distancia.x * 0.3f) ; 
    }

    Vector2 posicionInicial  ;

    private int movimientoTouchEnPantalla()
    { 
        int movimiento = 0;

        if (Input.touchCount > 0) {

            Touch toque = Input.GetTouch(0);

            float mitadPantalla = Screen.width / 2 ;

            if ( toque.position.x > mitadPantalla  ) {
                movimiento = 1;
            } else {
                movimiento = -1 ;
            }

        }
         

        return movimiento;
    }

    private int movimientoTouch () {

        int movimiento = 0;

        if ( Input.touchCount > 0) {

            Touch toque = Input.GetTouch(0);

            if ( toque.phase == TouchPhase.Began ) {
                posicionInicial = toque.position;
            }
            if ( toque.phase == TouchPhase.Moved ) {

                Vector2 posicionActual = toque.position ;

                if (posicionActual.x < posicionInicial.x ) { movimiento = -1; }
                if (posicionActual.x > posicionInicial.x) { movimiento = 1; }
            }
        } 
         
        return movimiento;
    }


    private void instanciarAsteroide() {

        int randomAsteroide = UnityEngine.Random.Range( 0, asteroidesFuente.Length );

        float random = UnityEngine.Random.Range( 0f , 1f ) ;
        Vector3 posicionAnchoAsteroide = Vector3.Lerp(limiteI.position, limiteD.position, random ) ;
        Vector3 posicionAsteroide = new Vector3(posicionAnchoAsteroide.x, 0, profundidadLejos.position.z) ;
        GameObject asteroide = Instantiate(asteroidesFuente[randomAsteroide], posicionAsteroide, Quaternion.identity);

        float randomDireccion = UnityEngine.Random.Range(0f, 1f);
        Vector3 posicionDireccionAsteroideAncho = Vector3.Lerp(limiteI.position, limiteD.position, randomDireccion);
        Vector3 posicionDireccionAsteroide = new Vector3(posicionDireccionAsteroideAncho.x, 0, 0 ) ;
        Vector3 direccionAsteroide = posicionDireccionAsteroide - posicionAsteroide ;
        float randomForce = UnityEngine.Random.Range(.1f, 2f); 
        asteroide.GetComponent<Rigidbody>().AddForce( Vector3.Normalize(direccionAsteroide * 2) * 2000 * randomForce ) ;
         
    }
}
