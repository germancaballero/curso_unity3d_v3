using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaveJugador : MonoBehaviour
{
    public int vida = 20 ;
    public GameObject explosion ;

    private ControladorJuegoNaves controladorJuegoNaves ;

    public void Start() {
        controladorJuegoNaves = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorJuegoNaves>();
    }

    private void OnTriggerEnter ( Collider colisionador ) {

        if (colisionador.tag == "Asteroide" ) {

            EnemigosAsteroides asteroide = colisionador.GetComponent<EnemigosAsteroides>();

            int daNo = asteroide.da�o ;
            vida = vida - daNo ;
            controladorJuegoNaves.cambiarVida(vida);

            GameObject explosionInstancia = Instantiate(explosion, this.transform.position , Quaternion.identity );
            Destroy(explosionInstancia, 4) ;
         
            if (vida <= 0) {

                Destroy(gameObject);

            }
        }
    }


}
