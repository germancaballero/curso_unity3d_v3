using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPlayers : MonoBehaviour
{
    private ControladorJuego controladorJuego;
    public GameObject playerFuente;
    public GameObject playerGrupo;

    private GameObject playerA;
    private GameObject playerB;

    public void Inicio()
    {
        controladorJuego = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorJuego>();
        playerA = Instantiate(playerFuente);
        playerB.transform.parent = playerGrupo.transform;
    }
    public void Actualizar()
    {
        if (true) {
            controladorJuego.controladorEnemigos.distanciaEnemigos();
        } 
    }
    public GameObject getPlayerB ()
    {
        return playerA;
    }
}
