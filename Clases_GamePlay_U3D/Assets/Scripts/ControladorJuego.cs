using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorJuego : MonoBehaviour
{
    public ControladorEnemigos controladorEnemigos;
    public ControladorPlayers controladorPlayers;
    public ControladorUI controladorUI;

    // Start is called before the first frame update
    void Start()
    { 
        controladorEnemigos.Inicio();
        controladorPlayers.Inicio();
        controladorUI.Inicio();
    }

    // Update is called once per frame
    void Update()
    {
        if (true) {
            controladorEnemigos.aNadirEnemigo();  
        }
    }
}
