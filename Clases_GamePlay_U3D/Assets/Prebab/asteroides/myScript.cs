using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myScript : MonoBehaviour
{

    Nave jugador ;
    Nave enemigo ;
    private void Start()
    {
         jugador = new Nave(0.5f , 10 , 20);
         enemigo = new Nave(1, 8, 10);

    }
    private void Update() {

        if (true) {
            jugador.atacar(enemigo); 
        }
    }
}

public class Nave {

    public float velocidad;
    public int daNo;
    public int vida;

    public Nave ( float velocidad, int daNo, int vida )
    {
        this.velocidad = velocidad;
        this.daNo = daNo;
        this.vida = vida;
    }

    public void atacar( Nave enemigo) {
        // funcion
        enemigo.vida = enemigo.vida - this.daNo ;
    }
}

